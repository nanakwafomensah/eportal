<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Loyalty Insurance | E-Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">

    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />

    <!--[if lte IE 9]>
    <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="assets/parsley/css/parsley.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
</head>
<body class="fixed-header" style = "background-color: #fff">
<div height=" 15px" style="background-color: #4A1459; color: #4A1459">f </div>
<div class="register-container full-height sm-p-t-30" style ="margin-top: -30px">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <center>  <img src="assets/img/loyal.png" alt="logo" data-src="assets/img/loyal.png" data-src-retina="assets/img/loyal.png" >
                    <h4 class="modal-title" style="color:#4A1459 ">PREMIUM AMOUNT CALCULATOR</h4>

                </center>
<br>
                <div>
                    <div class="row">
                        <div class="col-lg-6">
                            <p>TYPE OF MOTOR :</p>
                            <select id="type_of_motor" class="form-control" placeholder="click tochoose an Option"  name="type_of_motor" style="font-size: 11px" required>
                                <option value="">---</option>
                                <option value="X.1">X.1</option>
                                <option value="X.4" >X.4</option>
                                <option value="TAXI" >TAXI</option>
                                <option value="HIRING_CARS" >HIRING CARS</option>
                                <option value="MINI_BUS" >MINI BUS</option>
                                <option value="MAXI_BUS" >MAXI BUS</option>
                                <option value="MOTOR_CYCLE" >MOTOR CYCLE</option>
                                <option value="AMBULANCE_HEARSE" >AMBULANCE / HEARSE</option>
                                <option value="OWN_GOODS_upto" >OWN GOODS Z.300 ( upto  - 3000 CC)</option>
                                <option value="OWN_GOODS_above" >OWN GOODS Z.300 ( Above- 3000 CC)</option>
                                <option value="ART">ART / TANKERS</option>
                                <option value="CARTAGE_upto" >GEN. CARTAGE Z. 301 (upto - 3000 CC)</option>
                                <option value="CARTAGE_above" >GEN. CARTAGE Z. 301 (Above - 3000 CC)</option>
                                <option value="802_site" >Z.802 ON SITE</option>
                                <option value="802_road" >Z.802 ON ROAD</option>
                                <option value="GW1_CLASS1" >GW1 CLASS1</option>
                                <option value="GW1_CLASS2" >GW1 CLASS2</option>
                                <option value="GW1_CLASS3" >GW1 CLASS3</option>



                            </select>
                        </div>
                        <div class="col-lg-6">
                            <p>TYPE OF COVER</p>
                            <select id="type_of_cover" class="form-control" placeholder="click tochoose an Option"  name="type_of_cover" style="font-size: 11px" required>
                                <option value="">---</option>
                                <option value="comprehensive">Motor Comprehensive</option>
                                <option value="motor_third_party" >Motor Third Party</option>
                                <option value="third_party_fire_theft" >Third Party(Fire and Theft) Only</option>


                            </select>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <p>CUBIC CAPACITY(%):</p>
                            <input  name="cc_loading" id="cc_loading"  class="form-control " type='number' max='100' required ><br>


                        </div>
                        <div class="col-lg-3">
                            <p>NUMBER OF SEATS</p>
                            <input  type='number'  name="no_of_seats" id="no_of_seats" placeholder="" class="form-control "  required ><br>


                        </div>
                        <div class="col-lg-3">
                             <p>RATE (%):</p>
                            <input disabled  name="rate" id="rate" placeholder="" class="form-control " type='number' max='100' required ><br>

                        </div>
                        <div class="col-lg-3">
                            <p>BASIC PREMIUM(GHS):</p>
                            <input disabled name="basic_premium" id="basic_premium"  class="form-control " type="text" required ><br>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <p>TPPD(LIMIT)</p>
                            <input  type='number'  name="limit" id="limit" placeholder="4" class="form-control "  required ><br>

                        </div>
                        <div class="col-lg-6">
                            <p>VALUE OF VEHICLE (GHS)</p>
                            <input  type='number'  name="value_of_vehicle" id="value_of_vehicle" placeholder="300000" class="form-control "  required ><br>

                        </div>
                    </div>

                    
                    <center><button type="button" id="calculate_premium" class="btn btn-default"><i class="fa fa-check"> GO</i></button></center>

                    <hr style="color:#4A1459">
                    <div class="row">
                        <div class="col-lg-12">
                            <p>PREMIUM AMOUNT</p>
                            <textarea col="6" row="1" id="premium_amount"  class="form-control "  placeholder="GHS _ _ _ _ _ _" disabled ></textarea>

                        </div>

                    </div>

                </div>
              


                <br><hr>
                <a href="pay" style="margin-top: 10px; background-color: #4A1459" class="btn" style="color:white;"><i class="fa fa-long-arrow-right" aria-hidden="true" style="color:white"></i><span style="color:white">PURCHASE INSURANCE</span></a>
            </div>
        </div>
    </div>
</div>

<!-- START OVERLAY -->

<!-- END OVERLAY -->
<!-- BEGIN VENDOR JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->

<script src="assets/parsley/js/parsley.min.js" type="text/javascript"></script>
<script>
    $(function()
    {
//        $('#sucess_message').hide();
        $('#form-register').validate();
    })
</script>
<script>
    $(document).on('click','.cashbackclick',function() {
        // alert("hey");

        $('#percentagevalueedit').val($(this).data('percentagevalue'));
        $('#identifieredit').val($(this).data('identifiervalue'));

    });

</script>
<script>
    //
    var text_input = $('#premium_amount');
    text_input.css("font-size", "40px");
    text_input.css("text-align", "center");
</script>
<script>
    $(document).on('click','#calculate_premium',function() {
        $('#type_of_cover,#no_of_seats,#limit,#value_of_vehicle').each(function(){
            $(this).parsley().validate();

            if($(this).parsley().isValid()){

                var type_of_cover = $('#type_of_cover').val();
                var no_of_seats = $('#no_of_seats').val();
                var limit = $('#limit').val();
                var value_of_vehicle = $('#value_of_vehicle').val();
                var rate = $('#rate').val();
                var cc_loading = $('#cc_loading').val();


                $.ajax({
                    url:'Pay/calculate_premium',
                    type:'post',
                    data:{
                        type_of_cover:type_of_cover,
                        no_of_seats:no_of_seats,
                        limit:limit,
                        value_of_vehicle:value_of_vehicle,
                        rate:rate,
                        cc_loading:cc_loading
                    },
                    beforeSend: function() {
                        // setting a timeout
                        $('#calculate_premium').html('computing....');
                    },
                    complete: function() {
                        $('#calculate_premium').html('GO');
                    },
                    success:function(data, jxhr){

                        $('#premium_amount').val(data);


                    },
                    error:function(){}
                });




            }
        });

    });

    $(document).on('change','#type_of_motor',function() {

        var type_of_motor=$('#type_of_motor').val();
        var type_of_cover=$('#type_of_cover').val();
        $.ajax({
            type:'POST',
            data:{type_of_motor:type_of_motor,
                type_of_cover:type_of_cover
                },
            url:'<?php echo site_url('Pay/Get_rate_basic_car_type'); ?>',
            success:function(result){
          
                var x=result.split("|");
                $('#rate').val(x[0]);
                $('#basic_premium').val(x[1]);



            }
        });
    });
    $(document).on('change','#type_of_cover',function() {

        var type_of_motor=$('#type_of_motor').val();
        var type_of_cover=$('#type_of_cover').val();
        $.ajax({
            type:'POST',
            data:{type_of_motor:type_of_motor,
                type_of_cover:type_of_cover
                },
            url:'<?php echo site_url('Pay/Get_rate_basic_car_type'); ?>',
            success:function(result){

                var x=result.split("|");
                $('#rate').val(x[0]);
                $('#basic_premium').val(x[1]);



            }
        });
    });
</script>
</body>
</html>