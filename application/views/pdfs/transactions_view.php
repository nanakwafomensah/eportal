<?php

$title= "<h4>Transactions between $from_date to $to_date</h4>" ;
$html="
        <div id='logo'>
          <p style='text-align:center;'><img src='assets/img/loyal.png' height='42' width='42'/></p>
         </div>";
$html.="<style>
                table {
                    border-collapse: collapse;
                    width: 100%;
                }

                th, td {
                    padding: 8px;
                    text-align: left;
                    border-bottom: 1px solid #ddd;
                }
                tr:nth-child(even){background-color: #f2f2f2}

                th {
                    background-color: darkviolet;
                    color: black;
                }
</style>";


$html.= "
                            <table id='browsetable' class='dynamicTable table table-striped table-bordered table-primary table-condensed' cellspacing='0'  width='100%'>
                            <thead>
                            <tr>
                            <tr style='background:silver'><th colspan='9'>" . $title . "</th></tr>
                            <tr>
                            <td>APPLICATION ID</td>
                            <td>PREMIUM AMOUNT</td>
                            <td>PERCENTAGE REWARD</td>
                            <td>DATE</td>
                            <td>REWARD NUMBER</td>
                            <td>MOBILE NETWORK</td>
                            <td>INSURANCE TYPE</td>
                            <td>REWARD TYPE</td>
                            <td>TOKEN</td>
                           
                            </tr>
                            </tr>
                            </thead><tbody>";


 foreach ($records as $record):

     $html.= "<tr>
                            <td>$record->application_id</td>
                            <td>$record->premium_amount</td>
                            <td>$record->percentage_reward</td>
                            <td>$record->date</td>
                            <td>$record->reward_number</td>
                            <td>$record->mobile_network</td>
                            <td>$record->insurance_type</td>
                            <td>$record->reward_type</td>
                            <td>$record->token</td>
                          

                            </tr>";

     endforeach;
$html.= "</tbody></table>";


echo $html;