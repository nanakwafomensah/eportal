<?php

$title= "<h4>Rewards between $from_date to $to_date</h4>" ;
$html="
        <div id='logo'>
          <p style='text-align:center;'><img src='assets/img/loyal.png' align='center'/></p>
         </div>";
$html.="<style>
                table {
                    border-collapse: collapse;
                    width: 100%;
                }

                th, td {
                    padding: 8px;
                    text-align: left;
                    border-bottom: 1px solid #ddd;
                }
                tr:nth-child(even){background-color: #f2f2f2}

                th {
                    background-color: darkviolet;
                    color: black;
                }
</style>";


$html.= "
                            <table id='browsetable' class='dynamicTable table table-striped table-bordered table-primary table-condensed' cellspacing='0'  width='100%'>
                            <thead>
                            <tr>
                            <tr style='background:silver'><th colspan='9'>" . $title . "</th></tr>
                            <tr>
                            <td>PIN</td>
                            <td>MM NETWORK</td>
                            <td>MM NUMBER</td>
                            <td>RECEIPT/CAR NUMBER </td>
                            <td>DONATION</td>
                            <td>REWARD</td>
                            <td>INSURANCE TYPE</td>
                            <td>AGENT PHONENUMBER</td>
                            <td>FUEL STATION</td>
                           
                           
                            </tr>
                            </tr>
                            </thead><tbody>";


foreach ($records as $record):

    $html.= "<tr>
                            <td>$record->agent_pin</td>
                            <td>$record->mm_network</td>
                            <td>$record->mm_number</td>
                            <td>$record->agent_receiptnumber</td>
                            <td>$record->donation_value</td>
                            <td>$record->reward</td>
                            <td>$record->insurance_type</td>
                            <td>$record->agent_phonenumber</td>
                            <td>$record->fuelstation</td>
                          

                            </tr>";

endforeach;
$html.= "</tbody>

<tbody>
<tr>                        <td>PIN</td>
                            <td>MM NETWORK</td>
                            <td>MM NUMBER</td>
                            <td>RECEIPT/CAR NUMBER </td>
                            <td>DONATION</td>
                            <td>REWARD</td>
                            <td>INSURANCE TYPE</td>
                            <td>AGENT PHONENUMBER</td>
                            <td>FUEL STATION</td>
                           
                            </tr>
</tbody>

</table>";


echo $html;