<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Loyalty Insurance | E-Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
    <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/parsley/css/parsley.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
</head>
<body class="fixed-header" style = "background-color: #fff">
<div height=" 15px" style="background-color: #4A1459; color: #4A1459">f </div>
<div class="register-container full-height sm-p-t-30" style ="margin-top: -30px">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div id="alert_message"></div>
            <div class="col-sm-12 col-sm-height col-middle">
                 <img src="assets/img/loyal.png" alt="logo" data-src="assets/img/loyal.png" data-src-retina="assets/img/loyal.png" >
                    <?php
                    if(isset($paymentMessage)){
                        echo '<div class="alert alert-success" role="alert">
                              <h4 class="alert-heading">Well done!</h4>
                              <p>'.$paymentMessage.'</p>
                              <hr>
                              <p class="mb-0">Cheers</p>
                            </div>';
                    }
                    else
                    {
                    echo '<form id="myformpayment"  data-parsley-validate="">';
                    if($aplication_number==""){

                        echo '
                                                    <h3>Enter your Application id to make payment</h3></center>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group form-group-default">
                                                                <label>Application Number </label>
                                                                <input type="text" name="application_number_notexist" id="application_number_notexist" placeholder="" class="form-control"   style=" color: #26428b ">
                                                                <div id="loader_data"></div>
                                                            </div>
                
                                                        </div>
                
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group form-group-default">
                                                                <label>Insurance Type </label>
                                                                <input type="text" name="insurance_type_notexist" id="insurance_type_notexist" placeholder="" class="form-control"  required  style=" color: #26428b ">
                                                            </div>
                                                        </div>
                
                                                    </div>
                
                                                    <div class="row">
                
                
                                                        <div class="col-sm-12">
                                                            <div class="form-group form-group-default">
                                                                <label>Premium amount#</label>
                                                                <input type="text" name="premium_amount" id="premium_amount"  class="form-control" style="color: #26428b; " >
                                                            </div>
                                                        </div>
                                                    </div>';

                    }
                    else{

                        echo ' <div class="alert alert-success alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Application Submitted Successfully!</strong> A reference id has been sent via your email.
                                    </div>
                                    <h3>Please Confirm The Details &amp Proceed With Payment</h3></center>
                
                                            <center><label><h2>Application Number: <?php echo $aplication_number;?></h2></label></center>
                                            <input name="application_number" id="application_number" type="hidden" value="'.$aplication_number.'"/>
                
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group form-group-default">
                                                        <label>Insurance Type </label>
                                                        <input type="text" name="insurance_type" id="insurance_type" placeholder="" class="form-control" value = "'. $insurance_type.'" required readonly="true" style=" color: #26428b ">
                                                    </div>
                                                </div>
                
                                            </div>
                
                                            <div class="row">
                
                
                                                <div class="col-sm-12">
                                                    <div class="form-group form-group-default">
                                                        <label>Premium amount#</label>
                                                        <input type="text" name="premium_amount" id="premium_amount" placeholder="0.00" class="form-control" required readonly="true" style="color: #26428b; " value="'. $premium_amount.'">
                                                    </div>
                                                </div>
                                            </div>';

                    }
                    ?><?php 

               echo' <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Reward Type</label>
                            <select class="form-control" name="reward_type" id="reward_type" required>
                                <option value="">----</option>
                                <option value="money">MOBILE MONEY</option>
                                <option value="airtime">AIRTIME</option>

                            </select></div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group form-group-default">
                            <label>Mobile Network Type</label>
                            <select class="form-control" name="mobile_network" id="mobile_network" required>
                                <option value="">----</option>
                                <option value="MTN">MTN</option>
                                <option value="AIR">AIRTEL</option>
                                <option value="TIG">TIGO</option>
                                <option value="VOD">VODAFONE</option>
                            </select></div>
                    </div>

                </div>
                <div class="row">


                    <div class="col-sm-12">
                        <div class="form-group form-group-default">
                            <label>Reward number</label>
                            <input type="text" name="reward_number" id="reward_number" placeholder="0267888888" class="form-control" required style="color: #26428b; " >
                        </div>
                    </div>

                </div>



                <hr>
                <center><button class="btn btn-success" id="makepayment"><a href=""></a> Click to make Payment</button></center>
               <!-- <div class="row">
                    <div class="col-sm-2">

                        <center><img src="assets/img/mtn.png" id="mtnpayment" style="cursor: pointer;"></center>


                    </div>

                    <div class="col-sm-2">

                        <center><img src="assets/img/airtel.png" id="airtelpayment" style="cursor: pointer;"></center>

                    </div>

                    <div class="col-sm-2">

                        <center><img src="assets/img/tigo.png" id="tigopayment" style="cursor: pointer;"></center>

                    </div>

                    <div class="col-sm-2">

                        <center><img src="assets/img/voda.png" id="vodafonepayment" style="cursor: pointer;"></center>

                    </div>

                    <div class="col-sm-2">

                        <center><img src="assets/img/visa.png" id="visapayment"  style="cursor: pointer;"></center>

                    </div>

                    <div class="col-sm-2">

                        <center><img src="assets/img/master.png" id="masterpayment" style="cursor: pointer;"></center>

                    </div>
                </div> -->


                </form>';


                } ?>
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <div class="modal fade" id="airtel" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Airtel Money</h4>
                                <img src="assets/img/airtel.png">
                                <form  action="Pay/clientpay" method="post" data-parsley-validate="">
                                <p>Please Enter The Airtel Number For Payment</p>
                                <input type="text" name="pass" placeholder="Airtel Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>

                                <input type="hidden" id="reward_numberairtel" name="reward_number"/>
                                <input type="hidden"  id="mobile_networkairtel" name="mobile_network"/>
                                <input type="hidden"  id="reward_typeairtel" name="reward_type"/>
                                <input type="hidden" id="insurance_typeairtel"  name="insurance_type"/>
                                <input type="hidden" id="premium_amountairtel"  name="premium_amount"/>
                                <input type="hidden" id="application_numberairtel"  name="application_number"/>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="mtn" role="dialog">
                    <div class="modal-dialog">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Pay with MTN Money</h4>
                                    <img src="assets/img/mtn.png">
                                <form  action="Pay/clientpay" method="post" data-parsley-validate="">
                                    <p>Please Enter The MTN Number For Payment</p>
                                    <input type="text" name="withdrawal_number" id="withdrawal_number" placeholder="MTN Number" class="form-control text-center" maxlength="10" pattern="[0-9]{10}" required ><br>
                                    <p>Please Enter The Amount Paying</p>
                                    <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>

                                    <input type="hidden" id="reward_numbermtn" name="reward_number"/>
                                    <input type="hidden"  id="mobile_networkmtn" name="mobile_network"/>
                                    <input type="hidden"  id="reward_typemtn" name="reward_type"/>
                                    <input type="hidden" id="insurance_typemtn"  name="insurance_type"/>
                                    <input type="hidden" id="premium_amountmtn"  name="premium_amount"/>
                                    <input type="hidden" id="application_numbermtn"  name="application_number"/>

                                    </div>

                                    <div class="modal-body">

                                    </div>
                                    <div class="modal-footer">
                                        <center><button type="submit"  class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                                    </div>
                                </form>
                            </div>




                    </div>
                </div>

                <div class="modal fade" id="thanksVISA" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Payment Successful</h4>
                                <img src="assets/img/tick.png">
                                <p>VISA transaction is in progress .Redirecting in 5 seconds.....</p>


                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: maroon; border-color: maroon"data-dismiss="modal" onclick="window.location.href='../index.html'"><i class="fa fa-close"> Close</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal fade" id="thanksmaster" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Payment Successful</h4>
                                <img src="assets/img/tick.png">
                                <p>MAster Card transaction is in progress .Redirecting in 5 seconds.....</p>


                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: maroon; border-color: maroon"data-dismiss="modal" onclick="window.location.href='../index.html'"><i class="fa fa-close"> Close</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="modal fade" id="voda" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Vodafone Cash</h4>
                                <img src="assets/img/voda.png">
                                <form  action="Pay/clientpay" method="post" data-parsley-validate="">
                                <p>Please Enter The Vodafone Number For Payment</p>
                                <input type="text" name="pass" placeholder="Vodafone Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>

                                <input type="hidden" id="reward_numbervoda" name="reward_number"/>
                                <input type="hidden"  id="mobile_networkvoda" name="mobile_network"/>
                                <input type="hidden"  id="reward_typevoda" name="reward_type"/>
                                <input type="hidden" id="insurance_typevoda"  name="insurance_type"/>
                                <input type="hidden" id="premium_amountvoda"  name="premium_amount"/>
                                <input type="hidden" id="application_numbervoda"  name="application_number"/>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="tigo" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Tigo Cash</h4>
                                <img src="assets/img/tigo.png">
                                <form  action="Pay/clientpay" method="post" data-parsley-validate="">
                                <p>Please Enter The Tigo Number For Payment</p>
                                <input type="text" name="pass" placeholder="Tigo Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>

                                <input type="hidden" id="reward_numbertigo" name="reward_number"/>
                                <input type="hidden"  id="mobile_networktigo" name="mobile_network"/>
                                <input type="hidden"  id="reward_typetigo" name="reward_type"/>
                                <input type="hidden" id="insurance_typetigo"  name="insurance_type"/>
                                <input type="hidden" id="premium_amounttigo"  name="premium_amount"/>
                                <input type="hidden" id="application_numbertigo"  name="application_number"/>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="VISA" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with VISA</h4>
                                <img src="assets/img/visa.png">
                                <p>Please Enter The VISA Details For Payment</p>
                                <div class = "row>">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Enter Amount</label>
                                            <input type="text" id="visa_amount" name="visa_amount" placeholder="10.00" class="form-control" required >
                                        </div>


                                    </div>



                                </div>





                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button  id="makepaymentvisa"  type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"  style="cursor: pointer;"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="master" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MasterCard</h4>
                                <img src="assets/img/master.png">
                                <p>Please Enter The MasterCard Details</p>
                                <div class = "row>">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Enter Amount:</label>
                                            <input type="text" id="masteramount" name="masteramount" placeholder="10.00" class="form-control" required >
                                        </div>


                                    </div>

                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" id="makepaymentmaster" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>


                <br><hr>
            </div>

        </div>
        <a href="welcome" data-toggle="tooltip" title="Home" style="margin-top: 10px; background-color: #2C2C46" class="btn" ><i class="fa fa-long-arrow-left" aria-hidden="true" style="color:white"></i>

        </a>
    </div>
</div>

<!-- START OVERLAY -->

<!-- END OVERLAY -->
<!-- BEGIN VENDOR JS -->


<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->

<script src="assets/parsley/js/parsley.min.js" type="text/javascript"></script>


<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script>

    $(document).on('click','#makepayment',function(e) {
        e.preventDefault();

        //validate textfield
        $('#myformpayment').parsley().validate();
        ////////////////////////////////////
        if($('#myformpayment').parsley().isValid()){

            var application_number=$('#application_number').val();
            var insurance_type=$('#insurance_type').val();
            var amount=$('#premium_amount').val();
            var reward_type=$('#reward_type').val();
            var mobile_network=$('#mobile_network').val();
            var reward_number=$('#reward_number').val();
            //location.href = "{{url('specialExcel/')}}" + '/'+ title + '/'+ date_from + '/'+ date_to;

            location.href = "http://184.168.147.165:81/api/eportalApi/ghpaycollection/index.php?" + 'appnumber='+ application_number + '&amount='+ amount + '&itemname='+ insurance_type+ '&rewardtype='+ reward_type;

//            $('#application_numbermtn').val(application_number);
//            $('#mobile_networkmtn').val(mobile_network);
//            $('#reward_typemtn').val(reward_type);
//            $('#insurance_typemtn').val(insurance_type);
//            $('#premium_amountmtn').val(premium_amount);
//            $('#reward_numbermtn').val(reward_number);

           // $('#mtn').modal('show');
        }
    });
    $(document).on('click','#airtelpayment',function(e) {
        e.preventDefault();
        //validate textfield
        $('#myformpayment').parsley().validate();
        ////////////////////////////////////
        if($('#myformpayment').parsley().isValid()){
            var application_number=$('#application_number').val();
            var insurance_type=$('#insurance_type').val();
            var premium_amount=$('#premium_amount').val();
            var reward_type=$('#reward_type').val();
            var mobile_network=$('#mobile_network').val();
            var reward_number=$('#reward_number').val();

            $('#application_numberairtel').val(application_number);
            $('#mobile_networkairtel').val(mobile_network);
            $('#reward_typeairtel').val(reward_type);
            $('#insurance_typeairtel').val(insurance_type);
            $('#premium_amountairtel').val(premium_amount);
            $('#reward_numberairtel').val(reward_number);

            $('#airtel').modal('show');
        }
    });
    $(document).on('click','#tigopayment',function(e) {
        e.preventDefault();
        //validate textfield
        $('#myformpayment').parsley().validate();
        ////////////////////////////////////
        if($('#myformpayment').parsley().isValid()){
            var application_number=$('#application_number').val();
            var insurance_type=$('#insurance_type').val();
            var premium_amount=$('#premium_amount').val();
            var reward_type=$('#reward_type').val();
            var mobile_network=$('#mobile_network').val();
            var reward_number=$('#reward_number').val();

            $('#application_numbertigo').val(application_number);
            $('#mobile_networktigo').val(mobile_network);
            $('#reward_typetigo').val(reward_type);
            $('#insurance_typetigo').val(insurance_type);
            $('#premium_amounttigo').val(premium_amount);
            $('#reward_numbertigo').val(reward_number);
            $('#tigo').modal('show');
        }
    });
    $(document).on('click','#vodafonepayment',function(e) {
        e.preventDefault();
        //validate textfield
        $('#myformpayment').parsley().validate();
        ////////////////////////////////////
        if($('#myformpayment').parsley().isValid()){
            var application_number=$('#application_number').val();
            var insurance_type=$('#insurance_type').val();
            var premium_amount=$('#premium_amount').val();
            var reward_type=$('#reward_type').val();
            var mobile_network=$('#mobile_network').val();
            var reward_number=$('#reward_number').val();

            $('#application_numbervoda').val(application_number);
            $('#mobile_networkvoda').val(mobile_network);
            $('#reward_typevoda').val(reward_type);
            $('#insurance_typevoda').val(insurance_type);
            $('#premium_amountvoda').val(premium_amount);
            $('#reward_numbervoda').val(reward_number);
            $('#voda').modal('show');
        }
    });
    $(document).on('click','#visapayment',function(e) {
        e.preventDefault();
        //validate textfield
        $('#myformpayment').parsley().validate();
        ////////////////////////////////////
        if($('#myformpayment').parsley().isValid()){
            $('#VISA').modal('show');
        }
    });
    $(document).on('click','#masterpayment',function(e) {
        e.preventDefault();
        //validate textfield
        $('#myformpayment').parsley().validate();
        ////////////////////////////////////
        if($('#myformpayment').parsley().isValid()){
            $('#master').modal('show');
        }
    });



    $(document).on('click','#makepaymentvisa',function(){
        $('#visa_amount').parsley().validate();
        if($('#visa_amount').parsley().isValid()){
            $.ajax({
                type:'POST',
                data:{
                    amount:$('#visa_amount').val()

                },
                url:'<?php echo site_url('Pay/callvisa_payment'); ?>',
                success:function(result){
                    if(result){
                        $('#VISA').modal('hide');
                        $('#thanksVISA').modal('show');
                        setTimeout(window.location=result, 5000)

                    }




                }
            });

        }
    });
    $(document).on('click','#makepaymentmaster',function(){
//        alert($("#masteramount").val());
        $('#masteramount').parsley().validate();
        if($('#masteramount').parsley().isValid()){
            $.ajax({
                type:'POST',
                data:{
                    amount:$('#masteramount').val()

                },
                url:'<?php echo site_url('Pay/callvisa_payment'); ?>',
                success:function(result){
                    if(result){
                        $('#master').modal('hide');
                        $('#thanksmaster').modal('show');
                        setTimeout(window.location=result, 5000)

                    }




                }
            });

        }

    });





///query for details if session is lost
    $('#application_number_notexist').on("keyup", function () {

        $.ajax({
            type:'POST',
            data:{
                application_number:$(this).val()

            },
            url:'<?php echo site_url('Pay/querymotordetails'); ?>',
            beforeSend: function() {
                // setting a timeout
                $('#loader_data').html('<img src="assets/img/loader.gif"  height="" width="">');

            },
            complete: function() {
                $('#loader_data').html('');
            },
            success:function(result){
                var data=result;

                if(data.indexOf("|") != -1){
                    var arr=data.split("|");
                    $('#insurance_type_notexist').val(arr[0]);
                    $('#premium_amount').val(arr[1]);

                }else{
                    $('#insurance_type_notexist').val("");
                    $('#premium_amount').val("");
                }



            }
        });
    });
</script>
</body>
</html>