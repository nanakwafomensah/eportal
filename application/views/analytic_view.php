<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Loyalty Insurance | E-Portal</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="assets/parsley/css/parsley.css" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="index.html">
                        <img class="brand-img" src="dist/img/loyal.png" alt="brand" height ="36px" width="72px"/>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <ul class="nav navbar-right top-nav pull-right">




                <li class="dropdown auth-drp">

                    <a href="#logout" class="dropdown-toggle pr-0" data-toggle="modal">  <span style = "font-size: 10px"> Welcome, <?php  if(isset($usersfullname)){echo $usersfullname;} ?> </span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>

                </li>
            </ul>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">

            <li class="navigation-header">
                <span >Data Analytics</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a class="active" href="analytic" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Transactions &amp Clients</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

            </li>
        <li><hr class="light-grey-hr mb-10"/></li>
            <li class="navigation-header">
                <span>Data Management</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="fa fa-bank -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Users Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">

                   
                    <li>
                        <a href="#createUser" data-toggle="modal" style = "font-size: 11px">Register A User</a>
                    </li>

                    <li>
                        <a href="userutil" style = "font-size: 11px">User Utilities</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr_1"><div class="pull-left"><i class="fa fa-paperclip  -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Applications Management</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr_1" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="motorapp"  style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="fireapp"  style = "font-size: 11px">FIre</a>
                    </li>


                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr_2"><div class="pull-left"><i class="fa fa-money  -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Pending Application</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr_2" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="motorapppend"  style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="fireapppend"  style = "font-size: 11px">FIre</a>
                    </li>


                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr_set"><div class="pull-left"><i class="fa fa-cog -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Settings</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr_set" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a href="#createcashback" data-toggle="modal" class="cashbackclick" data-percentagevalue="<?php echo $percentage;?>" data-identifiervalue="<?php echo $identifervalue;?>" style = "font-size: 11px">Cash bank Settings</a>
                    </li>
                    <li>
                        <a href="rewardsetting"  style = "font-size: 11px">Reward Settings</a>
                    </li>
                    <li>
                        <a href="comprehensivesetting"  style = "font-size: 11px">Comprehensive Settings</a>
                    </li>
                    <li>
                        <a href="indexmotorthirdparty"  style = "font-size: 11px">Motor Party Settings</a>
                    </li>
                    <li>
                        <a href="indexmotorthirdfireparty"  style = "font-size: 11px">fire & Theft Settings</a>
                    </li>



                </ul>
            </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Reports</span>
            <i class="zmdi zmdi-more"></i>
        </li>
            <li>
                <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dra"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Completed Transaction</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dra" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a class="active" href="transactionmotor" style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="transactionfire"  style = "font-size: 11px">Fire </a>
                    </li>

                </ul>
            </li>
            <li>
                <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_drb"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">UnCompleted Transaction</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_drb" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a class="active" href="uncompleted_transactionmotor" style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="uncompleted_transactionfire"  style = "font-size: 11px">Fire </a>
                    </li>

                </ul>
            </li>
            <li>

                <a href="auditlog" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Audit trails</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                <a href="requestedcalls" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Requested Calls &nbsp;<span class="badge" style="background-color: #2C2C46;color: white;"><?php echo $numreq;?></span></span></div><div class="pull-right"></div><div class="clearfix"></div></a>

            </li>
            <li>
                <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_drs"><div class="pull-left"><i class="fa fa-trophy -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Rewards</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_drs" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a class="active" href="airtimeRewards" style = "font-size: 11px">Airtime </a>
                    </li>
                    <li>
                        <a href="mobileMoneyCashback"  style = "font-size: 11px"> Mobile Money </a>
                    </li>

                    <li>
                        <a href="fuelcoupons" style = "font-size: 11px">Coupons</a>
                    </li>

                </ul>
            </li>

        </ul>
    </div>
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->

    <!-- /Right Sidebar Menu -->

    <!-- Right Setting Menu -->


    <!-- Right Sidebar Backdrop -->
    <div class="right-sidebar-backdrop"></div>
    <!-- /Right Sidebar Backdrop -->

    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">

            <!-- Row -->
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-green" >
                                    <div class="container-fluid">
                                        <div class="row" style="background-color: #4A1459">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px">3</span></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >Registered Insurance Products</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-bank txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-red">
                                    <div class="container-fluid">
                                        <div class="row" style="background-color: #4A1459">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px"><?php echo $no_of_backendusers;?></span></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px">Backend Users</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-users txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box" style = "background-color: gainsboro">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-dark block counter"><span class="counter-anim" style = "font-size: 20px"><?php echo $no_of_attempted_payment;?></span></span>
                                                <span class="weight-500 uppercase-font txt-dark block font-13" style = "font-size: 18px">Today[GHS]</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-money txt-dark data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box" style = "background-color: gainsboro">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-dark block counter"><span class="counter-anim" style = "font-size: 20px"> <?php echo $revenue;?></span></span>
                                                <span class="weight-500 uppercase-font txt-dark block font-13" style = "font-size: 18px"> Year To Date [GHS]</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-money txt-dark data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /Row -->

            <!-- Row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark" style = "font-size: 12px">User Status Distribution</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <canvas id="chart_7" height="40"></canvas>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

            <div class="row">


                <div class="col-lg-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark" style = "font-size: 12px">Transaction Statistics</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in" style = "font-size: 8px">
                            <div id="morris_extra_bar_chart" class="morris-chart"  style = "font-size: 8px"></div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
    <!-- /Row -->

    <!-- Row -->
    <div class="modal fade" id="createUser" role="dialog">
        <div class="modal-dialog">
            <form action="storeuser" method="post" data-parsley-validate="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Create A User</h4>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label  class="form-label" style="font-size: 12px">User's Full Name</label>
                                <input id="form-control-6" class="form-control" type="text" style="font-size: 11px" name="fullname" required=""></div>

                            <div class="col-md-4">
                                <label  class="form-label" style="font-size: 12px">Sex</label>

                                <select id="demo-select2-2" class="form-control" style="font-size: 11px" name="sex">
                                    <option value="M" >Male</option>
                                    <option value="F">Female</option>
                                    <option value="contributor">Other</option>

                                </select>
                            </div>

                        </div></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <label  class="form-label" style="font-size: 12px">User Email</label>
                                <input id="form-control-7" class="form-control" type="email" name="email" style="font-size: 11px" required="">
                            </div>


                            <div class="col-md-4">
                                <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                <input id="form-control-10" class="form-control" type="text" name="phonenumber" style="font-size: 11px" required="" maxlength="10" pattern="[0-9]{10}">
                            </div>

                        </div>
                    </div><hr style="border-color: #4A1459">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label  class="form-label" style="font-size: 12px">User Role</label>
                                <select id="demo-select2-3" class="form-control" name="userrole" style="font-size: 11px">

                                    <option value="admin" >Admin</option>
                                    <option value="customersupport" >Customer Support</option>
                                    <option value="underwriters" >Underwriters</option>

                                </select>
                            </div>



                        </div>
                    </div>

                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <center>

                        <button type="submit" class="btn btn-primary" style = "background-color: #4A1459; border-color: #4A1459"><i class="icon icon-save"></i> Save</button>

                    </center>
                </div>
            </div>
           </form>
        </div>
    </div>
    <div class="modal fade" id="createcashback" role="dialog">
        <div class="modal-dialog">
            <form action="storepercentage" method="post" data-parsley-validate="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cash Back</h4>
                      <input type="hidden" id="identifieredit" name="identifieredit" />
                    <div class="form-group">
                        <div class="row">
                           <center> <div class="col-md-12">
                                <label  class="form-label" style="font-size: 12px">Percentage value (%)</label>
                                <input id="percentagevalueedit" class="form-control"  style="font-size: 11px" name="percentagevalue"  required  type='number' max='100'
                                     ></div>



                        </div></center>
                    </div>
                  </div>

                <div class="modal-footer">
                    <center>

                        <button type="submit" class="btn btn-primary" style = "background-color: #4A1459; border-color: #4A1459"><i class="icon icon-save"></i> Save</button>

                    </center>
                </div>
            </div>
           </form>
        </div>
    </div>
   
    <div class="modal fade" id="logout" role="dialog">
        <div class="modal-dialog">
            <form action="logout" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are you sure you want to logout</h4>

                    <p></p>

                </div>

                <div class="modal-footer">
                    <center>
                        <button type="submit" class="btn btn-danger" style = "background-color: #4A1459; border-color: #4A1459">OK</button>
                        <button type="button" class="btn btn-danger" style = "background-color: #4A1459; border-color: #4A1459">Cancel</button>
                        </center>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Data table JavaScript -->
<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- simpleWeather JavaScript -->
<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<!--<script src="dist/js/morris-data.js"></script>-->
<script>
    var x=2;
    $.ajax({
        url: '<?php echo site_url('Analytics/transaction_graph_montly'); ?>',
        type: 'post',
        data: {
            value: x

        },

        success: function (data, status) {
            console.log(data);
            var res = data.split("|");
            var jan_payment= parseInt(res[0]);
            var jan_actedon= parseInt(res[1]);
            var jan_pending= parseInt(res[2]);

            var feb_payment= parseInt(res[3]);
            var feb_actedon= parseInt(res[4]);
            var feb_pending= parseInt(res[5]);

            var mar_payment= parseInt(res[6]);
            var mar_actedon= parseInt(res[7]);
            var mar_pending= parseInt(res[8]);

            var apr_payment= parseInt(res[9]);
            var apr_actedon= parseInt(res[10]);
            var apr_pending= parseInt(res[11]);

            var may_payment= parseInt(res[12]);
            var may_actedon= parseInt(res[13]);
            var may_pending= parseInt(res[14]);

            var jun_payment= parseInt(res[15]);
            var jun_actedon= parseInt(res[16]);
            var jun_pending= parseInt(res[17]);

            var jul_payment= parseInt(res[18]);
            var jul_actedon= parseInt(res[19]);
            var jul_pending= parseInt(res[20]);

            var aug_payment= parseInt(res[21]);
            var aug_actedon= parseInt(res[22]);
            var aug_pending= parseInt(res[23]);

            var sep_payment= parseInt(res[24]);
            var sep_actedon= parseInt(res[25]);
            var sep_pending= parseInt(res[26]);

            var oct_payment= parseInt(res[27]);
            var oct_actedon= parseInt(res[28]);
            var oct_pending= parseInt(res[29]);

            var nov_payment= parseInt(res[30]);
            var nov_actedon= parseInt(res[31]);
            var nov_pending= parseInt(res[32]);

            var dec_payment= parseInt(res[33]);
            var dec_actedon= parseInt(res[34]);
            var dec_pending= parseInt(res[35]);
            //alert(dec_pending);
            ///////////////////////////////////////////////////
            if($('#morris_extra_bar_chart').length > 0)
            // Morris bar chart
                Morris.Bar({
                    element: 'morris_extra_bar_chart',
                    data: [{
                        y: 'Jan',
                        TotalPayments: jan_payment,
                        ActedOn: jan_actedon,
                        Pending: jan_pending
                    }, {
                        y: 'Feb',
                        TotalPayments: feb_payment,
                        ActedOn: feb_actedon,
                        Pending: feb_pending
                    }, {
                        y: 'Mar',
                        TotalPayments: mar_payment,
                        ActedOn: mar_actedon,
                        Pending: mar_pending
                    }, {
                        y: 'Apr',
                        TotalPayments: apr_payment,
                        ActedOn: apr_actedon,
                        Pending: apr_pending
                    }, {
                        y: 'May',
                        TotalPayments: may_payment,
                        ActedOn: may_actedon,
                        Pending: may_pending
                    },{
                        y: 'Jun',
                        TotalPayments: jun_payment,
                        ActedOn: jun_actedon,
                        Pending: jun_pending
                    }, {
                        y: 'Jul',
                        TotalPayments: jul_payment,
                        ActedOn: jul_actedon,
                        Pending: jul_pending
                    }, {
                        y: 'Aug',
                        TotalPayments: aug_payment,
                        ActedOn: aug_actedon,
                        Pending: aug_pending
                    }, {
                        y: 'Sep',
                        TotalPayments: sep_payment,
                        ActedOn: sep_actedon,
                        Pending: sep_pending
                    }, {
                        y: 'Oct',
                        TotalPayments: oct_payment,
                        ActedOn: oct_actedon,
                        Pending: oct_pending
                    }, {
                        y: 'Nov',
                        TotalPayments: nov_payment,
                        ActedOn: nov_actedon,
                        Pending: nov_pending
                    }, {
                        y: 'Dec',
                        TotalPayments: dec_payment,
                        ActedOn: dec_actedon,
                        Pending: dec_pending
                    }],
                    xkey: 'y',
                    ykeys: ['TotalPayments', 'ActedOn', 'Pending'],
                    labels: ['TotalPayments', 'ActedOn', 'Pending'],
                    barColors:['#4A1459', 'grey', '#896a09'],
                    hideHover: 'auto',
                    gridLineColor: '#878787',
                    resize: true,
                    gridTextColor:'#878787',
                    gridTextFamily:"Roboto",
                    gridTextSize: '10'
                });


            ///////////////////////////////////////////////
        }
    });
</script>
<!-- Sparkline JavaScript -->
<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<!--<script src="dist/js/chartjs-data.js"></script>-->
<script>
    var x=2;
    $.ajax({
        url: '<?php echo site_url('Analytics/active_inactive'); ?>',
        type: 'post',
        data: {
            value: x

        },

        success: function (data, status) {
          //  alert(data);
            var res = data.split("|");
            var active = res[0];
            var inactive = res[1];

            if( $('#chart_7').length > 0 ){
                var ctx7 = document.getElementById("chart_7").getContext("2d");
                var x= active;
                var y= inactive;
                var data7 = {
                    labels: [

                        "Active",
                        "NonActive"
                    ],
                    datasets: [
                        {

                            data: [ x,y ],
                            backgroundColor: [

                                "grey",
                                "#4A1459"
                            ],
                            hoverBackgroundColor: [

                                "grey",
                                "#4A1459"
                            ]
                        }]
                };


                var doughnutChart = new Chart(ctx7, {
                    type: 'doughnut',
                    data: data7,
                    options: {
                        animation: {
                            duration:	3000
                        },
                        responsive: true,
                        legend: {
                            labels: {
                                fontFamily: "Roboto",
                                fontColor:"#878787"
                            }
                        },
                        tooltips: {
                            backgroundColor:'rgba(33,33,33,1)',
                            cornerRadius:0,
                            footerFontFamily:"'Roboto'"
                        },
                        elements: {
                            arc: {
                                borderWidth: 0
                            }
                        }
                    }
                });
            }
        }
    });
</script>
<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Switchery JavaScript -->
<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>
<script src="dist/js/dashboard-data.js"></script>
<script src="assets/parsley/js/parsley.min.js"></script>
<script>
    $(document).on('click','.cashbackclick',function() {
       // alert("hey");

        $('#percentagevalueedit').val($(this).data('percentagevalue'));
        $('#identifieredit').val($(this).data('identifiervalue'));

    });
</script>
</body>

</html>
