<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Loyalty Insurance | E-Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/parsley/parsley.css" type="text/css">
    <!--[if lte IE 9]>
    <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
    <style>
        div.accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        div.accordion.active, button.accordion:hover {
            background-color: #ddd;
        }

        div.panel {
            padding: 0 18px;
            display: none;
            background-color: white;
        }
    </style>
</head>
<body class="fixed-header" style = "background-color: #fff">
<!--<div height=" 15px" style="background-color: #4A1459; color: #4A1459">f </div>-->
<div class="register-container" >

        <div class="row ">
            <div class="">
                <center>  <img src="assets/img/loyal.png" alt="logo" data-src="assets/img/loyal.png" data-src-retina="assets/img/loyal.png" >
                    <h3>MOTOR INSURANCE PROPOSAL FORM</h3>
                  <p>Please complete this form in block letters.please attach separate sheet(s),if required</p></center>
                <form method="post" action="Pay/storemotorinsurance" data-parsley-validate="" >
                <div class="accordion" style="border-bottom-style: solid;border-color: #2C2C46"> <span class="label label-default" style="background-color:#2C2C46; color: white">Step 1</span>  PROPOSER'S DETAILS</div>
                <div class="panel">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Name </label>
                                <input type="text" name="name" placeholder="Ama" class="form-control"   style=" color: #26428b " required>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Date of Birth:</label>
                                <input type="date" name="dob" placeholder="Ama" class="form-control"  style=" color: #26428b " required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Mobile Number:</label>
                                <input type="text" name="mobilenumber" placeholder="0236734363" class="form-control"   style=" color: #26428b " maxlength="10" pattern="[0-9]{10}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Occupation</label>
                                <input type="text" name="occupation" placeholder="" class="form-control"  style="color: #26428b; " required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Postal Address</label>
                                <input type="text" name="posaddress" placeholder="" class="form-control"   style="color: #26428b; " required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Physical Address</label>
                                <input type="text" name="phyaddress" placeholder="" class="form-control"  style="color: #26428b; " required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="text" name="email" placeholder="" class="form-control"  style="color: #26428b; "  required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>Tel:</label>
                                <input type="text" name="tel" placeholder="" class="form-control"  style="color: #26428b; " maxlength="10" pattern="[0-9]{10}" required >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Period of Insurance </label>

                            </div>
                            <div class="col-sm-5 form-group form-group-default">
                                From: <input type="date" name="from_" placeholder="" class="form-control"  style="color: #26428b; " required >
                            </div>
                            <span>&nbsp;</span>
<!--                            <div class="col-sm-2 form-group form-group-default">-->
<!--                                At: <input type="text" name="at" placeholder="" class="form-control"   style="color: #26428b; "  required>-->
<!--                            </div>-->

                            <div class="col-sm-5 form-group form-group-default">
                                To: <input type="date" name="to_" placeholder="" class="form-control"   style="color: #26428b; " required>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Type of Cover</label>
                                <select id="typeOfCover" class="form-control" placeholder="click tochoose an Option"  name="type_of_cover" style="font-size: 11px" required>
                                    <option value="">---</option>
                                    <option value="comprehensive">Motor Comprehensive</option>
                                    <option value="motor_third_party" >Motor Third Party</option>
                                    <option value="third_party_fire_theft" >Third Party(Fire and Theft) Only</option>



                                </select>
                               
                            </div>
                        </div>
                    </div>
             
                </div>

                <div class="accordion"  style="border-bottom-style: solid;border-color: #2C2C46"><span class="label label-default" style="background-color:#2C2C46; color: white">Step 2</span> PARTICULARS OF VEHICLE TO BE INSURED</div>
                <div class="panel " ></br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>TPPD(limit)</label>
                                <input type="text" name="tppd" placeholder="" class="form-control"  style="color: #26428b; " required >
                            </div>
                        </div>
                    </div>

                    <table width="100%">
                        <tr>
                            <th><center>Make&Model</center></th>
                            <th><center>Type of Body</center></th>
                            <th><center>cubic capacity</center></th>
                            <th><center>no of seats</center></th>

                        </tr>
                        <tr>
                            <td><select id="make_model" class="form-control" placeholder="click tochoose an Option"  name="make_model" style="font-size: 11px" required>
                                    <option value="">---</option>
                                    <option value="X.1">X.1</option>
                                    <option value="X.4" >X.4</option>
                                    <option value="TAXI" >TAXI</option>
                                    <option value="HIRING_CARS" >HIRING CARS</option>
                                    <option value="MINI_BUS" >MINI BUS</option>
                                    <option value="MAXI_BUS" >MAXI BUS</option>
                                    <option value="MOTOR_CYCLE" >MOTOR CYCLE</option>
                                    <option value="AMBULANCE_HEARSE" >AMBULANCE / HEARSE</option>
                                    <option value="OWN_GOODS_upto" >OWN GOODS Z.300 ( upto  - 3000 CC)</option>
                                    <option value="OWN_GOODS_above" >OWN GOODS Z.300 ( Above- 3000 CC)</option>
                                    <option value="ART">ART / TANKERS</option>
                                    <option value="CARTAGE_upto" >GEN. CARTAGE Z. 301 (upto - 3000 CC)</option>
                                    <option value="CARTAGE_above" >GEN. CARTAGE Z. 301 (Above - 3000 CC)</option>
                                    <option value="802_site" >Z.802 ON SITE</option>
                                    <option value="802_road" >Z.802 ON ROAD</option>
                                    <option value="GW1_CLASS1" >GW1 CLASS1</option>
                                    <option value="GW1_CLASS2" >GW1 CLASS2</option>
                                    <option value="GW1_CLASS3" >GW1 CLASS3</option>



                                </select>
                                <input name="rate_1" id="rate_1" hidden/>
                                <input name="basic_premium_1" id="basic_premium_1" hidden/>
                            </td>





                            <td><input type="text"  class="form-control" name="type_of_body" required></td>
                            <td><input  class="form-control" type='number' max='100' name="cubic_capacity" required></td>
                            <td><input  class="form-control" type='number'  name="no_of_seats" required></td>

                        </tr>

                    </table>
                    <table   >
                        <tr>

                            <th><center>Year of manufacture</center></th>
                            <th><center>Value of vehicle</center></th>
                            <th><center>Value of accessories</center></th>
                            <th><center>Reg No.</center></th>
                        </tr>
                        <tr>

                            <td><input type="text" class="form-control" name="year_of_manufacture" required></td>
                            <td><input type="text" class="form-control" name="value_of_vehicle" required></td>
                            <td><input type="text" class="form-control" name="value_of_accessories" required></td>
                            <td><input type="text" class="form-control" name="reg_no" required></td>

                        </tr>

                    </table><br>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group form-group-default">
                                <label>CHASSIS NO.</label>
                                <input type="text" name="chassis_no" placeholder="" class="form-control" required   style=" color: #26428b ">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-group-default">
                                <label>ENGINE NO.</label>
                                <input type="text" name="engine_no" placeholder="" class="form-control" required  style=" color: #26428b ">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="accordion"  style="border-bottom-style: solid;border-color: #2C2C46"><span class="label label-default" style="background-color:#2C2C46; color: white">Step 3</span> ABOUT THE DRIVERS</div>
                <div class="panel"><br>
                    <div class="row">
                        <div class="col-sm-6">
                        1.  Is the driving License under 12 months?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about1" class="about1btn" value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about1" class="about1btn" value="No">&nbsp;No</div>
                            <div class="col-sm-2"><input type="text" class="form-control " name="about1text" id="#about1btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                          2.  Use of Vehicle?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about2"  value="Yes">&nbsp;Commercial</div>
                            <div class="col-sm-2"><input type="radio" name="about2" value="No">&nbsp;Private</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about2text" id="#about2btntext"  ></div>

                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                         3.  Does anyone else drive your vehicle on regular basis?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about3"  value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about3" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about3text" id="#about3btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                         4.Has any insurer declined,cancelled or imposed special trerms on or refused to renew your policy?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about4"  value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about4" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about4text" id="#about4btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            5.  Have u been involved in a Motor accident in the past 3 years?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about5"  value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about5" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about5text" id="#about5btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            6.  Does anyone who drive the vehicle have any physical infirmities ?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about6"  value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about6" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about6text" id="#about6btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            7.  Are you entitiled to a No claim Discount from your previous insurers ?
                        </div>

                        <div class="col-sm-2"><input type="radio" name="about7"  value="Yes">&nbsp;Yes</div>
                        <div class="col-sm-2"><input type="radio" name="about7" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about7text" id="#about7btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            8.  Will your vehicle be used to carry fare paying passengers?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about8"  value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about8" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about8text" id="#about8btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                          9. Are passengers carried incidental to a contract for conbveyance of goods or merchandise?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about9"  value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about9" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about9text" id="#about9btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            10.  Will your vehicle be used  for carriage of goods?
                        </div>

                            <div class="col-sm-2"><input type="radio" name="about10"  value="Yes">&nbsp;Yes</div>
                            <div class="col-sm-2"><input type="radio" name="about10" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about10text" id="#about10btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            11.  Do you undertake the carriage of goods for other persons for reward?
                        </div>

                        <div class="col-sm-2"><input type="radio" name="about11"  value="Yes">&nbsp;Yes</div>
                        <div class="col-sm-2"><input type="radio" name="about11" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about11text" id="#about11btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            12.  Is your vehicle subject to loan?
                        </div>

                        <div class="col-sm-2"><input type="radio" name="about12"  value="Yes">&nbsp;Yes</div>
                        <div class="col-sm-2"><input type="radio" name="about12" value="No">&nbsp;No</div>
                        <div class="col-sm-2"><input type="text" class="form-control " name="about12text" id="#about12btntext"  ></div>


                    </div>&nbsp;
                    <div class="row">
                        <div class="col-sm-6">
                            13.  Whose name is the vehicle registered?
                        </div>

                        <div class="col-sm-2"><input type="radio" name="about13"  value="Yes" hidden></div>
                        <div class="col-sm-2"><input type="radio" name="about13" value="No" hidden></div>
                        <div class="col-sm-6"><input type="text" class="form-control " name="about13text" id="#about13btntext" required ></div>


                    </div>&nbsp;
                </div>
                <div class="accordion"  style="border-bottom-style: solid;border-color: #2C2C46"><span class="label label-default" style="background-color:#2C2C46; color: white">Step 4</span> DECLARATION</div>
                <div class="panel">
                    <p>In addition to any other details supplied to Loyalty Insurance Company Limited, I
                      We, the undersigned, hereby declare that to the best of my/our knowledge and belief, the information given by me/us is true and complete and that all material information has been disclosed and i/we agree that this application shall be the basis of the contract between me/us and Loyalty. I/We understand and accept that Loyalty reserves the right to accept or reject a proposal to their discretion. I/We will give notice to Loyalty of any changes in the information relating to the insured, as stated above, I/We agree to accept a policy in Loyalty's usual form for this class of insurance and pay the premium thereon</p>
                     <div class="row">

                         <div class="col-md-4">
                             <input type="checkbox" id="check" name="agreement"  value="Yes">I Agree<br>


                         </div>

                     </div>
                      <div>
                          <input type="submit"  id="btncheck" class="btn " style = "color:white;background-color: forestgreen; border-color: forestgreen" value="Send"/>
                      </div>
                </div>

                </form>
                <div class="slack_button_container" style="height: 90px;">

                </div>

<!--                <hr>Pay With-->
<!--                    <div class="row">-->
<!--                        <div class="col-sm-2">-->
<!---->
<!--                            <center><img src="assets/img/mtn.png" data-toggle="modal" data-target="#mtn" style="cursor: pointer;"></center>-->
<!---->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div class="col-sm-2">-->
<!---->
<!--                            <center><img src="assets/img/airtel.png" data-toggle="modal" data-target="#airtel" style="cursor: pointer;"></center>-->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div class="col-sm-2">-->
<!---->
<!--                            <center><img src="assets/img/tigo.png" data-toggle="modal" data-target="#tigo" style="cursor: pointer;"></center>-->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div class="col-sm-2">-->
<!---->
<!--                            <center><img src="assets/img/voda.png" data-toggle="modal" data-target="#voda" style="cursor: pointer;"></center>-->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div class="col-sm-2">-->
<!---->
<!--                            <center><img src="assets/img/visa.png" data-toggle="modal" data-target="#VISA" style="cursor: pointer;"></center>-->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div class="col-sm-2">-->
<!---->
<!--                            <center><img src="assets/img/master.png" data-toggle="modal" data-target="#master" style="cursor: pointer;"></center>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->



                <div class="modal fade" id="airtel" role="dialog">
                    <div class="modal-dialog">
                        <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Airtel Money</h4>
                                <img src="assets/img/airtel.png">
                                <p>Please Enter The Airtel Number For Payment</p>
                                <input type="text" name="mobile_number" placeholder="Airtel Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount"  class="form-control text-center" required maxlength="10"  required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"><br>
                                <input type="hidden" name="paymentmedium"  class="form-control text-center" value="airtel"><br>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                          </form>
                    </div>
                </div>

                <div class="modal fade" id="mtn" role="dialog">
                    <div class="modal-dialog">
                        <form action="payCreate" method="post">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Pay with MTN Money</h4>
                                    <img src="assets/img/mtn.png">
                                    <p>Please Enter The MTN Number For Payment</p>
                                    <input type="text" name="mobile_number" placeholder="MTN Number" class="form-control text-center" required maxlength="10"><br>
                                    <p>Please Enter The Amount Paying</p>
                                  <input type="text" name="amount"  class="form-control text-center" required maxlength="10"  required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"><br>
                                    <input type="hidden" name="paymentmedium"  class="form-control text-center" value="mtn"><br>
                                </div>
                                <div class="modal-body">

                                </div>
                                <div class="modal-footer">
                                    <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <div class="modal fade" id="thanksVISA" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Payment Successful</h4>
                                <img src="assets/img/tick.png">
                                <p>VISA transaction has been processed successfully</p>


                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: maroon; border-color: maroon"data-dismiss="modal" onclick="window.location.href='../../index.html'"><i class="fa fa-close"> Close</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="modal fade" id="voda" role="dialog">
                    <div class="modal-dialog">
                        <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Vodafone Cash</h4>
                                <img src="assets/img/voda.png">
                                <p>Please Enter The Vodafone Number For Payment</p>
                                <input type="text" name="mobile_number" placeholder="Vodafone Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="10.00" class="form-control text-center" required ><br>
                                <input type="hidden" name="paymentmedium"  class="form-control text-center" value="voda"><br>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
</form>
                    </div>
                </div>

                <div class="modal fade" id="tigo" role="dialog">
                    <div class="modal-dialog">
                        <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Tigo Cash</h4>
                                <img src="assets/img/tigo.png">
                                <p>Please Enter The Tigo Number For Payment</p>
                                <input type="text" name="mobile_number" placeholder="Tigo Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="12.00" class="form-control text-center" required ><br>
                                <input type="hidden" name="paymentmedium"  class="form-control text-center" value="tigo"><br>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
</form>
                    </div>
                </div>

                <div class="modal fade" id="VISA" role="dialog">
                    <div class="modal-dialog">
                        <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with VISA</h4>
                                <img src="assets/img/visa.png">
                                <p>Please Enter The VISA Details For Payment</p>
                                <div class = "row>">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-default">
                                            <label>VISA Number</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Paying Amount</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                </div>

                                <div class = "row>">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>CVV</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Month</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Year</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                    <input type="hidden" name="paymentmedium"  class="form-control text-center" value="visa"><br>


                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen" data-dismiss="modal" data-toggle="modal" data-target="#thanksVISA" style="cursor: pointer;"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                       </form>
                    </div>
                </div>

                <div class="modal fade" id="master" role="dialog">
                    <div class="modal-dialog">
                        <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MasterCard</h4>
                                <img src="assets/img/master.png">
                                <p>Please Enter The MasterCard Details</p>
                                <div class = "row>">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-default">
                                            <label>MasterCard Number</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Paying Amount</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                </div>

                                <div class = "row>">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>CVV</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Month</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Year</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <input type="hidden" name="paymentmedium"  class="form-control text-center" value="master"><br>

                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>


                <br><hr>
            </div>
            <a href="welcome" style="margin-top: 10px; background-color: #2C2C46" class="btn" ><i class="fa fa-long-arrow-left" aria-hidden="true" style="color:white"></i>

            </a>
        </div>

</div>

<!-- START OVERLAY -->

<!-- END OVERLAY -->
<!-- BEGIN VENDOR JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<script>
    $(function()
    {
        $("#if_text :input").attr("disabled", true);
        $('#form-register').validate()

    })
</script>
<script>
    $(document).on('click','.cashbackclick',function() {
        // alert("hey");

        $('#percentagevalueedit').val($(this).data('percentagevalue'));
        $('#identifieredit').val($(this).data('identifiervalue'));

    });
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }
</script>
<script>

    $('#check').change(function () {
        $('#btncheck').prop("disabled", !this.checked);
    }).change()



    $(document).on('change','#make_model',function() {

        var type_of_motor=$('#make_model').val();
        var type_of_cover=$('#typeOfCover').val();
        $.ajax({
            type:'POST',
            data:{type_of_motor:type_of_motor,
                   type_of_cover :type_of_cover
            },
            url:'<?php echo site_url('Pay/Get_rate_basic_car_type'); ?>',
            success:function(result){
                //alert(result);
                var x=result.split("|");
                $('#rate_1').val(x[0]);
                $('#basic_premium_1').val(x[1]);


            }
        });
    });
    $(document).on('change','#typeOfCover',function() {

        var type_of_motor=$('#make_model').val();
        var type_of_cover=$('#typeOfCover').val();
        $.ajax({
            type:'POST',
            data:{type_of_motor:type_of_motor,
                type_of_cover :type_of_cover
            },
            url:'<?php echo site_url('Pay/Get_rate_basic_car_type'); ?>',
            success:function(result){
                //alert(result);
                var x=result.split("|");
                $('#rate_1').val(x[0]);
                $('#basic_premium_1').val(x[1]);


            }
        });
    });
</script>



</body>
</html>