<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Loyalty Insurance | E-Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/parsley/parsley.css" type="text/css">
    <!--[if lte IE 9]>
    <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
    <style>
        div.accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        div.accordion.active, button.accordion:hover {
            background-color: #ddd;
        }

        div.panel {
            padding: 0 18px;
            display: none;
            background-color: white;
        }
    </style>
</head>
<body class="fixed-header" style = "background-color: #fff">
<!--<div height=" 15px" style="background-color: #4A1459; color: #4A1459">f </div>-->
<div class="register-container" >

    <div class="row ">
        <div class="">
            <center>  <img src="assets/img/loyal.png" alt="logo" data-src="assets/img/loyal.png" data-src-retina="assets/img/loyal.png" >
                <h3>FIRE INSURANCE PROPOSAL FORM</h3>
                <p>Please complete this form in block letters.please attach separate sheet(s),if required</p></center>
            <form method="post" action="Pay/storefireinsurance">
                <div class="accordion" style="border-bottom-style: solid;border-color: #2C2C46"> <span class="label label-default" style="background-color:#2C2C46; color: white">Step 1</span>  PROPOSER'S DETAILS</div>
                <div class="panel">
                        <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Fullname of Proposer</label>
                                        <input type="text" name="name"  class="form-control"  required  style=" color: #26428b ">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Postal Address:</label>
                                        <input type="text" name="address"  class="form-control" required style=" color: #26428b ">
                                    </div>
                                </div>
                            </div>
                        <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group form-group-default">
                                        <label>Tel:</label>
                                        <input type="text" name="tel" placeholder="" class="form-control"  required  style=" color: #26428b ">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-group-default">
                                        <label>Email:</label>
                                        <input type="email" name="email" placeholder="" class="form-control" required  style=" color: #26428b ">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-group-default">
                                        <label>Fax:</label>
                                        <input type="text" name="fax" placeholder="" class="form-control"   style=" color: #26428b ">
                                    </div>
                                </div>
                            </div>
                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>www:</label>
                                    <input type="text" name="www" placeholder="" class="form-control"   style=" color: #26428b ">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Occupation:</label>
                                    <input type="text" name="occupation" placeholder="" class="form-control" required   style=" color: #26428b ">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Contact Person:</label>
                                    <input type="text" name="contact_person"  class="form-control" required  style=" color: #26428b ">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Mobile:</label>
                                    <input type="text" name="mobile" placeholder="" class="form-control" required   style=" color: #26428b ">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>Where is the property situated:</label>
                                    <input type="text" name="property_situated" placeholder="" class="form-control" required   style=" color: #26428b ">
                                </div>
                            </div>

                        </div>

                        <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>What is the naure of construction?</label>
                                        <input type="text" name="nature_of_contstruction" placeholder="" required class="form-control"   style="color: #26428b; " >
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group form-group-default">
                                        <label>Describe in full the nature of activity to be carried out in the premises:</label>
                                        <textarea name="activity_premises" rows="3" required style="min-width: 100%" ></textarea>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <label>Have you ever suffered any loss by fire? </label>
                                        <input type="radio" name="coprehensive_cover" value="yes">Yes
                                        <input type="radio" name="coprehensive_cover" value="yes">No
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group form-group-default">
                                        <textarea name="sufferred_loss_by_fire" rows="3"  style="min-width: 100%" ></textarea>
                                   </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>11.	Has any insurance company ever cancelled, refused to renew or accept your business only on special terms?</label>
                                    <input type="radio" name="coprehensive_cover" value="yes">Yes
                                    <input type="radio" name="coprehensive_cover" value="yes">No
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <textarea name="insurance_acceptance" rows="3"  style="min-width: 100%" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Is the property proposed for insurance already insured with another company?  </label>
                                    <input type="radio" name="coprehensive_cover" value="yes">Yes
                                    <input type="radio" name="coprehensive_cover" value="yes">No
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <textarea name="insured_by_another_company" rows="3"  style="min-width: 100%" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <label>Is your property subject to a mortgage? </label>
                                    <input type="radio" name="coprehensive_cover" value="yes">Yes
                                    <input type="radio" name="coprehensive_cover" value="yes">No
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-group-default">
                                    <textarea name="mortgage" rows="3"  style="min-width: 100%" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group form-group-default">
                                    <label>Period of Insurance</label>

                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group form-group-default">
                                <label>From:</label>
                                <div class="form-group form-group-default">
                                    <input type="date" name="from_" placeholder="" class="form-control"   style="color: #26428b; " >

                                </div>
                                    </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group form-group-default">
                                <label>To</label>
                                <div class="form-group form-group-default">
                                    <input type="date" name="to_" placeholder="" class="form-control"   style="color: #26428b; " >

                                </div></div>
                            </div>
                        </div>
                </div>

                <div class="accordion"  style="border-bottom-style: solid;border-color: #2C2C46"><span class="label label-default" style="background-color:#2C2C46; color: white">Step 4</span> DECLARATION</div>

                <div class="panel">
                    <p>In addition to any other details supplied to LOYALTY INSURANCE COMPANY LIMITED, I/We, the undersigned, declare that to the best of my/our knowledge and belief the information given by me/us is true and complete and that all material information has been disclosed and I/we agree that this application shall be the basis of the contract between me/us and LOYALTY.  I/We understand and accept that LOYALTY reserves the right to accept or reject a proposal at their discretion.  I/We will give notice to LOYALTY of any change in the information relating to the insured, as stated above.  I/We agree to accept a policy in LOYALTY’s usual form for this class of insurance and pay the premium thereon.</p>
                    <div class="row">

                        <div class="col-md-4">
                            <input type="checkbox" id="check" name="agreement"  value="Yes"> I Agree<br>


                        </div>

                    </div>
                    <div>
                        <input type="submit"  id="btncheck" class="btn " style = "color:white;background-color: forestgreen; border-color: forestgreen" value="Send"/>
                    </div>
                </div>

            </form>
            <div class="slack_button_container" style="height: 90px;">
                <a href="welcome" style="margin-top: 10px; background-color: #2C2C46" class="btn" ><i class="fa fa-long-arrow-left" aria-hidden="true" style="color:white"></i>

                </a>
            </div>
<!--            <hr>Pay With-->
<!--            <div class="row">-->
<!--                <div class="col-sm-2">-->
<!---->
<!--                    <center><img src="assets/img/mtn.png" data-toggle="modal" data-target="#mtn" style="cursor: pointer;"></center>-->
<!---->
<!---->
<!--                </div>-->
<!---->
<!--                <div class="col-sm-2">-->
<!---->
<!--                    <center><img src="assets/img/airtel.png" data-toggle="modal" data-target="#airtel" style="cursor: pointer;"></center>-->
<!---->
<!--                </div>-->
<!---->
<!--                <div class="col-sm-2">-->
<!---->
<!--                    <center><img src="assets/img/tigo.png" data-toggle="modal" data-target="#tigo" style="cursor: pointer;"></center>-->
<!---->
<!--                </div>-->
<!---->
<!--                <div class="col-sm-2">-->
<!---->
<!--                    <center><img src="assets/img/voda.png" data-toggle="modal" data-target="#voda" style="cursor: pointer;"></center>-->
<!---->
<!--                </div>-->
<!---->
<!--                <div class="col-sm-2">-->
<!---->
<!--                    <center><img src="assets/img/visa.png" data-toggle="modal" data-target="#VISA" style="cursor: pointer;"></center>-->
<!---->
<!--                </div>-->
<!---->
<!--                <div class="col-sm-2">-->
<!---->
<!--                    <center><img src="assets/img/master.png" data-toggle="modal" data-target="#master" style="cursor: pointer;"></center>-->
<!---->
<!--                </div>-->
<!--            </div>-->



            <div class="modal fade" id="airtel" role="dialog">
                <div class="modal-dialog">
                    <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Airtel Money</h4>
                                <img src="assets/img/airtel.png">
                                <p>Please Enter The Airtel Number For Payment</p>
                                <input type="text" name="mobile_number" placeholder="Airtel Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount"  class="form-control text-center" required maxlength="10"  required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"><br>
                                <input type="hidden" name="paymentmedium"  class="form-control text-center" value="airtel"><br>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="mtn" role="dialog">
                <div class="modal-dialog">
                    <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MTN Money</h4>
                                <img src="assets/img/mtn.png">
                                <p>Please Enter The MTN Number For Payment</p>
                                <input type="text" name="mobile_number" placeholder="MTN Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount"  class="form-control text-center" required maxlength="10"  required data-parsley-pattern="^[0-9]*\.[0-9]{2}$" placeholder="1.00"><br>
                                <input type="hidden" name="paymentmedium"  class="form-control text-center" value="mtn"><br>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div class="modal fade" id="thanksVISA" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Payment Successful</h4>
                            <img src="assets/img/tick.png">
                            <p>VISA transaction has been processed successfully</p>


                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <center><button type="button" class="btn btn-danger" style = "background-color: maroon; border-color: maroon"data-dismiss="modal" onclick="window.location.href='../../index.html'"><i class="fa fa-close"> Close</i></button></center>
                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="voda" role="dialog">
                <div class="modal-dialog">
                    <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Vodafone Cash</h4>
                                <img src="assets/img/voda.png">
                                <p>Please Enter The Vodafone Number For Payment</p>
                                <input type="text" name="mobile_number" placeholder="Vodafone Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="10.00" class="form-control text-center" required ><br>
                                <input type="hidden" name="paymentmedium"  class="form-control text-center" value="voda"><br>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="tigo" role="dialog">
                <div class="modal-dialog">
                    <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Tigo Cash</h4>
                                <img src="assets/img/tigo.png">
                                <p>Please Enter The Tigo Number For Payment</p>
                                <input type="text" name="mobile_number" placeholder="Tigo Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="12.00" class="form-control text-center" required ><br>
                                <input type="hidden" name="paymentmedium"  class="form-control text-center" value="tigo"><br>

                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="VISA" role="dialog">
                <div class="modal-dialog">
                    <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with VISA</h4>
                                <img src="assets/img/visa.png">
                                <p>Please Enter The VISA Details For Payment</p>
                                <div class = "row>">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-default">
                                            <label>VISA Number</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Paying Amount</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                </div>

                                <div class = "row>">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>CVV</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Month</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Year</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                    <input type="hidden" name="paymentmedium"  class="form-control text-center" value="visa"><br>


                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen" data-dismiss="modal" data-toggle="modal" data-target="#thanksVISA" style="cursor: pointer;"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="master" role="dialog">
                <div class="modal-dialog">
                    <form action="payCreate" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MasterCard</h4>
                                <img src="assets/img/master.png">
                                <p>Please Enter The MasterCard Details</p>
                                <div class = "row>">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-default">
                                            <label>MasterCard Number</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Paying Amount</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                </div>

                                <div class = "row>">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>CVV</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Month</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Year</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <input type="hidden" name="paymentmedium"  class="form-control text-center" value="master"><br>

                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <br><hr>
        </div>

    </div>

</div>

<!-- START OVERLAY -->

<!-- END OVERLAY -->
<!-- BEGIN VENDOR JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->
<script>
    $(function()
    {
        $('#form-register').validate()
    })
</script>
<script>
    $(document).on('click','.cashbackclick',function() {
        // alert("hey");

        $('#percentagevalueedit').val($(this).data('percentagevalue'));
        $('#identifieredit').val($(this).data('identifiervalue'));

    });
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }
</script>
<script>
    $('#check').change(function () {
        $('#btncheck').prop("disabled", !this.checked);
    }).change()
</script>
</body>
</html>