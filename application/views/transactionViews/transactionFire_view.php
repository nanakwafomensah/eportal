<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Loyalty Insurance | E-Portal</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="assets/parsley/css/parsley.css" rel="stylesheet" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="index.html">

                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <ul class="nav navbar-right top-nav pull-right">




                <li class="dropdown auth-drp">
                    <a href="#logout" class="dropdown-toggle pr-0" data-toggle="modal">  <span style = "font-size: 10px"> Welcome, <?php  if(isset($usersfullname)){echo $usersfullname;} ?> </span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">

            <li class="navigation-header">
                <span >Data Analytics</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a class="active" href="analytic" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Transactions &amp Clients</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

            </li>
            <li><hr class="light-grey-hr mb-10"/></li>
            <li class="navigation-header">
                <span>Data Management</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr"><div class="pull-left"><i class="fa fa-bank -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Users Mangement</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">

                   
                    <li>
                        <a href="#createUser" data-toggle="modal" style = "font-size: 11px">Register A User</a>
                    </li>

                    <li>
                        <a href="userutil" style = "font-size: 11px">User Utilities</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr_1"><div class="pull-left"><i class="fa fa-paperclip  -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Applications Mangement</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr_1" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="motorapp"  style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="fireapp"  style = "font-size: 11px">FIre</a>
                    </li>


                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr_2"><div class="pull-left"><i class="fa fa-money  -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Payments Mangement</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr_2" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="motorapppend"  style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="fireapppend"  style = "font-size: 11px">FIre</a>
                    </li>


                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr_set"><div class="pull-left"><i class="fa fa-cog -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Settings</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr_set" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a href="#createcashback" data-toggle="modal" class="cashbackclick" data-percentagevalue="<?php echo $percentage;?>" data-identifiervalue="<?php echo $identifervalue;?>" style = "font-size: 11px">Cash bank Settings</a>
                    </li>
                    <li>
                        <a href="rewardsetting"  style = "font-size: 11px">Reward Settings</a>
                    </li>
                    <li>
                        <a href="comprehensivesetting"  style = "font-size: 11px">Comprehensive Settings</a>
                    </li>
                    <li>
                        <a href="indexmotorthirdparty"  style = "font-size: 11px">Motor Party Settings</a>
                    </li>
                    <li>
                        <a href="indexmotorthirdfireparty"  style = "font-size: 11px">fire & Theft Settings</a>
                    </li>



                </ul>
            </li>
            <li><hr class="light-grey-hr mb-10"/></li>
            <li class="navigation-header">
                <span>Reports</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dra"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Completed Transaction</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dra" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a class="active" href="transactionmotor" style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="transactionfire"  style = "font-size: 11px">Fire </a>
                    </li>

                </ul>
            </li>
            <li>
                <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_drb"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">UnCompleted Transaction</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_drb" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a class="active" href="uncompleted_transactionmotor" style = "font-size: 11px">Motor</a>
                    </li>
                    <li>
                        <a href="uncompleted_transactionfire"  style = "font-size: 11px">Fire </a>
                    </li>

                </ul>
            </li>
            <li>

                <a href="auditlog" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Audit trails</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
                <a href="requestedcalls" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="fa fa-th-list mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Requested Calls</span></div><div class="pull-right"></div><div class="clearfix"></div></a>

            </li>
            <li>
                <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_drs"><div class="pull-left"><i class="fa fa-trophy -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Rewards</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_drs" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a class="active" href="airtimeRewards" style = "font-size: 11px">Airtime </a>
                    </li>
                    <li>
                        <a href="mobileMoneyCashback"  style = "font-size: 11px"> Mobile Money </a>
                    </li>

                    <li>
                        <a href="fuelcoupons" style = "font-size: 11px">Coupons</a>
                    </li>

                </ul>
            </li>

        </ul>
    </div>
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->

    <!-- /Right Sidebar Menu -->

    <!-- Right Setting Menu -->


    <!-- Right Sidebar Backdrop -->
    <div class="right-sidebar-backdrop"></div>
    <!-- /Right Sidebar Backdrop -->

    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">

            <!-- Row -->

            <!-- /Row -->

            <!-- Row -->
            <div class="row" >
<!-- use of angular js-->
                <form action="Report/transcompletedquery" method="post">
                <div class="col-sm-2">
                    <label class="control-label mb-10 text-left" style="font-size: 11px;">From </label>
                    <input type="date"  class="form-control" name="dateFrom" required style="font-size: 11px;">
                </div>
                <div class="col-sm-2">
                    <label class="control-label mb-10 text-left" style="font-size: 11px;">To</label>
                    <input type="date"  class="form-control" name="dateTo" required style="font-size: 11px;">
                </div>
                <div class="col-sm-2">
                    <button type="submit"  class="btn btn-success  pull-right btn-xs " style = " ; background-color: forestgreen; border-color: forestgreen; margin-top: 40px"><i class="fa fa-database"></i><span class="btn-text"> Generate Data</span></button>
                </div>
               </form>

            </div>
            <br><hr>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box bg-red">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px" id="_total_money"><?echo $total_money?></span></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >GHS (Total)</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-money txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box" style="background-color: #808080">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px" id="_total_acted_on"><?php echo $acted_on;?></span></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >GHS [Confirmed]</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-money txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div></div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="panel panel-default card-view pa-0">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body pa-0">
                                <div class="sm-data-box" style="background-color: maroon">
                                    <div class="container-fluid">
                                        <div class="row" style="background-color: #896a09">
                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                <span class="txt-light block counter"><span class="counter-anim" style = "font-size: 20px" id="_total_pending_on"><?php echo  $pending ?></span></span>
                                                <span class="weight-500 uppercase-font txt-light block font-13" style = "font-size: 18px" >GHS [Unconfirmed]</span>
                                            </div>
                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                <i class="fa fa-money txt-light data-right-rep-icon"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div></div>

            </div>


            <div class="row">


                <div class="col-lg-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">
                            <div class="pull-left">
                                <h6 class="panel-title txt-dark" style = "font-size: 12px">Transaction Statistics</h6>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in" style = "font-size: 8px">
<!--                            <div id="morris_extra_bar_chart2" class="morris-chart"  style = "font-size: 8px"></div>-->
                        </div>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap" style = "font-size: 12px">
                                    <div class="">
                                        <table id="myTable1" class="table table-hover display  pb-30" style = "font-size: 12px">
                                            <thead style = "font-size: 12px">
                                            <tr style = "font-size: 12px">
                                                <th>APPLICATION ID</th>
                                                <th>PAYEES NAME</th>
                                                <th>PAYMENT TOKEN</th>
                                                <th>PHONE NUMBER</th>

                                                <th>AMOUNT</th>
                                                <th>DATE</th>
                                                <th>TIME</th>
                                                <th>PAYMENT MODE</th>
                                                <th>ACTION</th>
<!--                                                <th>Action</th>-->
                                            </tr>
                                            </thead>
                                            <tfoot style = "font-size: 12px">
                                            <tr style = "font-size: 12px">
                                                <th>APPLICATION ID</th>
                                                <th>PAYEES NAME</th>
                                                <th>PAYMENT TOKEN</th>
                                                <th>PHONE NUMBER</th>

                                                <th>AMOUNT</th>
                                                <th>DATE</th>
                                                <th>TIME</th>
                                                <th>PAYMENT MODE</th>
                                                <th>ACTION</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            <?php foreach ($records as $record):?>
                                               
                                                <tr>
                                                    <td><?php echo strtoupper($record->application_id); ?></td>
                                                    <td><?php echo strtoupper($record->name); ?></td>
                                                    <td><?php echo strtoupper($record->token); ?></td>
                                                    <td><?php echo strtoupper($record->withdrawal_number); ?></td>

                                                    <td><?php echo strtoupper($record->premium_amount); ?></td>
                                                    <td><?php
                                                         $date= New DateTime($record->date);
                                                        echo strtoupper($date->format('d-m-Y')); ?></td>
                                                    <td><?php echo strtoupper($record->time); ?></td>

                                                   <td><?php echo strtoupper($record->mobile_network);?> </td>
                                                    <td>
                                                        <?php
                                                        if($record->isconfirmed == 'yes'){
                                                           echo '<button type="submit" class="btn btn-success  pull-middle btn-xs " style = "background-color: forestgreen;border-color: forestgreen "><span class="btn-text">CONFIRMED</span></button>';
                                                        }else{
                                                            echo '<button type="submit" class="btn btn-success  pull-middle btn-xs " style = "background-color: goldenrod;border-color: goldenrod "><span class="btn-text">CONFIRM</span></button>';

                                                        }


                                                        ?>


                                                    </td>
                                                </tr>

                                            <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

    <!-- Row -->
    <!-- Row -->
    <div class="modal fade" id="createUser" role="dialog">
        <div class="modal-dialog">
            <form action="storeuser" method="post" data-parsley-validate="">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Create A User</h4>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <label  class="form-label" style="font-size: 12px">User's Full Name</label>
                                    <input id="form-control-6" class="form-control" type="text" style="font-size: 11px" name="fullname" required=""></div>

                                <div class="col-md-4">
                                    <label  class="form-label" style="font-size: 12px">Sex</label>

                                    <select id="demo-select2-2" class="form-control" style="font-size: 11px" name="sex">
                                        <option value="M" >Male</option>
                                        <option value="F">Female</option>
                                        <option value="contributor">Other</option>

                                    </select>
                                </div>

                            </div></div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <label  class="form-label" style="font-size: 12px">User Email</label>
                                    <input id="form-control-7" class="form-control" type="email" name="email" style="font-size: 11px" required="">
                                </div>


                                <div class="col-md-4">
                                    <label  class="form-label" style="font-size: 12px">Phone Number</label>
                                    <input id="form-control-10" class="form-control" type="text" name="phonenumber" style="font-size: 11px" required="" maxlength="10" pattern="[0-9]{10}">
                                </div>

                            </div>
                        </div><hr style="border-color: #4A1459">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label  class="form-label" style="font-size: 12px">User Role</label>
                                    <select id="demo-select2-3" class="form-control" name="userrole" style="font-size: 11px">

                                        <option value="admin" >Admin</option>
                                        <option value="customersupport" >Customer Support</option>
                                        <option value="underwriters" >Underwriters</option>

                                    </select>
                                </div>



                            </div>
                        </div>

                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <center>

                            <button type="submit" class="btn btn-primary" style = "background-color: #4A1459; border-color: #4A1459"><i class="icon icon-save"></i> Save</button>

                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="createcashback" role="dialog">
        <div class="modal-dialog">
            <form action="storepercentage" method="post" data-parsley-validate="">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cash Back</h4>
                        <input type="hidden" id="identifieredit" name="identifieredit" />
                        <div class="form-group">
                            <div class="row">
                                <center> <div class="col-md-12">
                                        <label  class="form-label" style="font-size: 12px">Percentage value (%)</label>
                                        <input id="percentagevalueedit" class="form-control"  style="font-size: 11px" name="percentagevalue"  required type='number' max='100'
                                        ></div>



                            </div></center>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <center>

                            <button type="submit" class="btn btn-primary" style = "background-color: #4A1459; border-color: #4A1459"><i class="icon icon-save"></i> Save</button>

                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="logout" role="dialog">
        <div class="modal-dialog">
            <form action="logout" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Are you sure you want to logout</h4>

                        <p></p>

                    </div>

                    <div class="modal-footer">
                        <center>
                            <button type="submit" class="btn btn-danger" style = "background-color: #4A1459; border-color: #4A1459">OK</button>
                            <button type="button" class="btn btn-danger" style = "background-color: #4A1459; border-color: #4A1459">Cancel</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Row -->
    <!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Data table JavaScript -->
<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- simpleWeather JavaScript -->
<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<script src="dist/js/morris-data.js"></script>

<!-- Sparkline JavaScript -->
<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<script src="dist/js/chartjs-data.js"></script>
<script src="assets/parsley/js/parsley.min.js"></script>
<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Switchery JavaScript -->
<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>
<script src="dist/js/dashboard-data.js"></script>

<script>
    $(document).on('click','.cashbackclick',function() {
        // alert("hey");

        $('#percentagevalueedit').val($(this).data('percentagevalue'));
        $('#identifieredit').val($(this).data('identifiervalue'));

    });
</script>


</body>

</html>
