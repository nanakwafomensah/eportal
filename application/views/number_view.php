<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Loyalty Insurance | E-Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/parsley/css/parsley.css" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />

    <!--[if lte IE 9]>
    <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
</head>
<body class="fixed-header" style = "background-color: #fff">
<div height=" 15px" style="background-color: #4A1459; color: #4A1459">f </div>
<div class="register-container full-height sm-p-t-30" style ="margin-top: -30px">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <center>  <img src="assets/img/loyal.png" alt="logo" data-src="assets/img/loyal.png" data-src-retina="assets/img/loyal.png" >
                    <h3>Please Enter Your Number</h3></center>

                    <div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Phone Number </label>
                                <input type="text" id="mobile_number" name="mobile_number" placeholder="024455566" class="form-control" maxlength="10" pattern="[0-9]{10}" required >
                            </div>
                        </div>

                    </div>
                    <center>   <button type="submit" id="confirm" class="btn btn-danger" style = "background-color: #4A1459; border-color: #4A1459" ><i class="fa fa-thumbs-up"> Confirm</i></button></center>


                </div>
                <div class="modal fade" id="confirm_number_page" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <i class="fa fa-smile-o fa-5x" style="color: #4A1459"></i>
                                <p></p>

                            </div>
                            <div class="modal-body">
                                <div id="mobile_numberconfirm_message"></div>
                                <div class="form-group form-group-default">
                                    <label> Confirm Phone Number </label>
                                    <input type="text" id="mobile_numberconfirm" name="mobile_numberconfirm" placeholder="024455566" class="form-control" maxlength="10" pattern="[0-9]{10}" required  style=" color: #26428b ">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <center><button type="button" id="ok"class="btn btn-danger" style = "background-color: #4A1459; border-color: #4A1459"><i> OK</i></button></a></center>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="mtn" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MTN Money</h4>
                                <img src="assets/img/mtn.png">
                                <p>Please Enter The MTN Number For Payment</p>
                                <input type="text" name="pass" placeholder="MTN Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="pass" placeholder="Airtel Number" class="form-control text-center" required maxlength="10"><br>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="thanksVISA" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Payment Successful</h4>
                                <img src="assets/img/tick.png">
                                <p>VISA transaction has been processed successfully</p>


                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: maroon; border-color: maroon"data-dismiss="modal" onclick="window.location.href='../index.html'"><i class="fa fa-close"> Close</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="modal fade" id="voda" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Vodafone Cash</h4>
                                <img src="assets/img/voda.png">
                                <p>Please Enter The Vodafone Number For Payment</p>
                                <input type="text" name="pass" placeholder="Vodafone Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="pass" placeholder="Airtel Number" class="form-control text-center" required maxlength="10"><br>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="tigo" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Tigo Cash</h4>
                                <img src="assets/img/tigo.png">
                                <p>Please Enter The Tigo Number For Payment</p>
                                <input type="text" name="pass" placeholder="Tigo Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="pass" placeholder="Airtel Number" class="form-control text-center" required maxlength="10"><br>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="VISA" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with VISA</h4>
                                <img src="assets/img/visa.png">
                                <p>Please Enter The VISA Details For Payment</p>
                                <div class = "row>">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-default">
                                            <label>VISA Number</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Paying Amount</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                </div>

                                <div class = "row>">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>CVV</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Month</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Year</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>


                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen" data-dismiss="modal" data-toggle="modal" data-target="#thanksVISA" style="cursor: pointer;"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="master" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MasterCard</h4>
                                <img src="assets/img/master.png">
                                <p>Please Enter The MasterCard Details</p>
                                <div class = "row>">
                                    <div class="col-sm-8">
                                        <div class="form-group form-group-default">
                                            <label>MasterCard Number</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Paying Amount</label>
                                            <input type="text" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>
                                </div>

                                <div class = "row>">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>CVV</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Month</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group form-group-default">
                                            <label>Expiring Year</label>
                                            <input type="number" name="pass" placeholder="" class="form-control" required >
                                        </div>


                                    </div>


                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"data-dismiss="modal"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>


                <br><hr>
            </div>
        </div>
    </div>
</div>

<!-- START OVERLAY -->

<!-- END OVERLAY -->
<!-- BEGIN VENDOR JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->

<script src="assets/parsley/js/parsley.min.js" type="text/javascript"></script>
<script>
    $(function()
    {
//        $('#sucess_message').hide();
        $('#form-register').validate();
    })
</script>
<script>
    $(document).on('click','.cashbackclick',function() {
        // alert("hey");

        $('#percentagevalueedit').val($(this).data('percentagevalue'));
        $('#identifieredit').val($(this).data('identifiervalue'));

    });
    $(document).on('click','#confirm',function(e) {

        $('#mobile_number').parsley().validate();
        if($('#mobile_number').parsley().isValid()){

            $('#confirm_number_page').modal("show");
        }
   });

    $(document).on('click','#ok',function(e) {
        $('#mobile_numberconfirm').parsley().validate();
        if($('#mobile_numberconfirm').parsley().isValid()){
            var value1=$('#mobile_number').val();
            var value2=$('#mobile_numberconfirm').val();


            if(value1==value2){
                //ajax

                $.ajax({
                    type: 'POST',
                    data: {
                        mobile_number: value2

                    },
                    url: '<?php echo site_url('Number/store'); ?>',
                    success: function (result){
                       if(result=='ok'){
                           $('#mobile_numberconfirm_message').html("<span style='color:green;'>Number Verification Successfull.Redirecting in 10 seconds....</span>");
                            setTimeout(function(){ window.location.href = "welcome"; }, 10000);
                       }

                    }
                });
            }else{

                $('#mobile_numberconfirm_message').html("<span style='color:red;'>Number confirmation failed</span>");
            }

        }


    });
</script>

</body>
</html>