<?php
Class User extends CI_Model
{
    function login($username, $password)
    {

        $this -> db -> select('id, useremail, logintoken,usersfullname,role');
        $this -> db -> from('users');
        $this -> db -> where('useremail', $username);
        $this -> db -> where('logintoken', $password);
        $this -> db -> where('status', 'A');
    
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
?>