<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: nana1
 * Date: 12/28/2017
 * Time: 6:05 AM
 */

if(!function_exists('auditlog'))
{
    function auditlog($userid,$event_type,$description)
    {
        $ci =& get_instance();
        $data = array(
            'userid' => $userid,
            'event_type ' => $event_type,
            'description'=>$description,
            'date'=>date("Y-m-d"),
            'time'=>date("h:i:s")

        );
        $ci->db->insert('audit_log', $data);


    }
}