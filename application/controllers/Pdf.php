<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

   

    //DOWNLOAD TRANSACTION PDF
    public function index($from_date,$to_date)
    {

        $from_date= preg_replace('/^%22/','',$from_date);
        $from_date= substr($from_date, 0, 10);

        $to_date= preg_replace('/^%22/','',$to_date);
        $to_date= substr($to_date, 0, 10);

        $this->db->select("*");
        $this->db->from("payments");
        $this->db->where('date >=',$from_date);
        $this->db->where('date >=',$to_date);
        $query = $this->db->get();

        $records['records'] = $query->result();
        $records['from_date']=$from_date;
        $records['to_date']=$to_date;

        //load the view and saved it into $html variable
        $html=$this->load->view('pdfs/transactions_view', $records, true);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "output_pdf_name.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
        $this->m_pdf->pdf->setHeader('{DATE j-m-Y}');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");


    }
    public function couponreward($from_date,$to_date){
        $from_date=$from_date;
        $to_date= $to_date;


        /////connecting to  the ussd database
        $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
        $query = $otherdb->query("select * from coupon_reward_reg  inner join insuranceagentpins on coupon_reward_reg.agent_pin =insuranceagentpins.agentpin where dateredeemed  >= ' $from_date' and dateredeemed  <= '$to_date'");
        $records['records']=$query->result();
        //////////////////////////////////////////////////////////

        $records['from_date']=$from_date;
        $records['to_date']=$to_date;

        //load the view and saved it into $html variable
        $html=$this->load->view('pdfs/couponpdfview', $records, true);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "couponpdfview.pdf";

        //load mPDF library
        $this->load->library('m_pdf');
     
        //generate the PDF from the given html

        $this->m_pdf->pdf->WriteHTML($html);


        $this->m_pdf->pdf->setHeader('{DATE j-m-Y}');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
    public function mm_cashback($from_date,$to_date){
        $from_date=$from_date;
        $to_date= $to_date;


        /////connecting to  the ussd database
        $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
        $query = $otherdb->query("select agent_pin,mm_network,mm_number,agent_receiptnumber,donation_value,reward,insurance_type,agent_phonenumber,insuranceagentpins.fuelstation from money_reward_reg
  inner join insuranceagentpins on money_reward_reg.agent_pin =insuranceagentpins.agentpin WHERE money_reward_reg.dategen >='$from_date' and money_reward_reg.dategen <='$to_date'");
        $records['records']=$query->result();
        //////////////////////////////////////////////////////////

        $records['from_date']=$from_date;
        $records['to_date']=$to_date;

        //load the view and saved it into $html variable
        $html=$this->load->view('pdfs/mm_cashback', $records, true);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "mm_cashback.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html

        $this->m_pdf->pdf->WriteHTML($html);


        $this->m_pdf->pdf->setHeader('{DATE j-m-Y}');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }
    public function airtime($from_date,$to_date){
        $from_date=$from_date;
        $to_date= $to_date;


        /////connecting to  the ussd database
        $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
        $query = $otherdb->query("select agent_pin,airtime_reward_network,airtime_reward_number,agent_receiptnumber,donation_value,reward,insurance_type,agent_phonenumber,insuranceagentpins.fuelstation from airtime_reward_reg
inner join insuranceagentpins on airtime_reward_reg.agent_pin =insuranceagentpins.agentpin WHERE airtime_reward_reg.dategen >='$from_date' and airtime_reward_reg.dategen <='$to_date'
");
        $records['records']=$query->result();
        //////////////////////////////////////////////////////////

        $records['from_date']=$from_date;
        $records['to_date']=$to_date;

        //load the view and saved it into $html variable
        $html=$this->load->view('pdfs/airtime', $records, true);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "airtime.pdf";

        //load mPDF library
        $this->load->library('m_pdf');

        //generate the PDF from the given html

        $this->m_pdf->pdf->WriteHTML($html);


        $this->m_pdf->pdf->setHeader('{DATE j-m-Y}');
        $this->m_pdf->pdf->setFooter('{PAGENO}');
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }




   
}
