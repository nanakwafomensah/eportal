<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Number extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
   
    public function index()
    {
        
        $this->load->view('number_view');
     

    }
    public function store(){
        $data = array(
            'number' => $this->input->post("mobile_number"),
            'status ' =>  'D',

            'date'=>date("Y-m-d"),
            'time'=>date("h:i:s")

        );
        $this->db->insert('numbers', $data);
        echo "ok";
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
