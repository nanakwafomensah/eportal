<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motorinsurance extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
///////////////////////////////////////////////////////////////////////////////////////////////
            $query = $this->db->query("select * from motor,payments where motor.application_status='approved' and motor.application_id  IN(select payments.application_id from payments) ");
            $records['records'] = $query->result();
    ///////////////////////////////////////////////////////////////////////////////////////////
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['numreq']=$this->Numreq();

            $this->load->view('motorinsu_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }



    }


    function indexpending(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
///////////////////////////////////////////////////////////////////////////////////////////////
            $query = $this->db->query("select * from motor,payments where motor.application_status='pending' and motor.application_id  IN(select payments.application_id from payments) ");
            $records['records'] = $query->result();
            ///////////////////////////////////////////////////////////////////////////////////////////
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['numreq']=$this->Numreq();
//            $records['total_money']=$this->get_total_amount();
//            $records['pending']=$this->get_total_amount_pending();
//            $records['acted_on']=$this->get_total_amount_acted_on();

            $this->load->view('motorinsupending_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }


    }
    
    function approvepending($application_id){


        $data = array(
            'application_status' => 'approved'

        );
        //print_r($data) ;
        $this->db->where('application_id', $application_id);
        $this->db->update('motor', $data);
        redirect('motorapppend');
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
