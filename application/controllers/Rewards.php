<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rewards extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }


    public function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select * from motor,payments where motor.application_id  IN(select payments.application_id from payments)");
            $records['records'] = $query->result();
            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;

        /////connecting to  the ussd database
            $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
            $query = $otherdb->query("select * from insuarancetype_values ");
            //var_dump($query->result());
            $records['rewards']=$query->result();
            $records['numreq']=$this->Numreq();
            $this->load->view('rewards_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }


    public function updatereward(){
        /////connecting to  the ussd database
        $id=$this->input->post("id");
        $rewardpercent=$this->input->post("rewardpercent");
        $donationpercent=$this->input->post("donationpercent");

        $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
        $query = $otherdb->query("UPDATE insuarancetype_values SET reward_percent = '$rewardpercent', donation_value= '$donationpercent' WHERE id = '$id' ");
        $_SESSION['success'] = 'success';
        redirect('rewardsetting');
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
