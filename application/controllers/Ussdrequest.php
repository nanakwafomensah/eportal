<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ussdrequest extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select * from motor,payments where motor.application_id  IN(select payments.application_id from payments)");
            $records['records'] = $query->result();
            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;

            /////connecting to  the ussd database
            $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
            $query = $otherdb->query("select * from airtime_reward_reg ");
            $records['airtime_records']=$query->result();
            $records['numreq']=$this->Numreq();
//////////////////////////////////////////////////////////
            $this->load->view('airtimeRewards_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    public function mobileMoneyCashback(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select * from motor,payments where motor.application_id  IN(select payments.application_id from payments)");
            $records['records'] = $query->result();
            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;

            /////connecting to  the ussd database
            $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
            $query = $otherdb->query("select * from money_reward_reg  ");
            $records['mm_records']=$query->result();
            //////////////////////////////////////////////////////////
            $records['numreq']=$this->Numreq();

            $this->load->view('mobileMoneyCashback_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    public function fuelcoupons(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select * from motor,payments where motor.application_id  IN(select payments.application_id from payments)");
            $records['records'] = $query->result();
            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;

            /////connecting to  the ussd database
            $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
            $query = $otherdb->query("select * from coupon_reward_reg  inner join insuranceagentpins on coupon_reward_reg.agent_pin =insuranceagentpins.agentpin ");
            $records['coupon_records']=$query->result();
            /////////////////////////////////
            //total redeemed
            $queryTq = $otherdb->query(" select count(*) as total from coupon_reward_reg where status= 'D' ");
            $queryT=$queryTq->row();
            $records['towardrewards']=$queryT->total;
          //total redeemed
            $queryIq = $otherdb->query(" select count(*) as total from coupon_reward_reg where status is not null ");
            $queryI=$queryIq->row();
            $records['towardissued']=$queryI->total;
            //////////////////////////////////////////////////////////
            $records['numreq']=$this->Numreq();

            $this->load->view('fuelcoupons_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }

}
