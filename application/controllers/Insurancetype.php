<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Insurancetype extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

////comprehensive
    public function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select * from motor,payments where motor.application_id  IN(select payments.application_id from payments)");
            $records['records'] = $query->result();
            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;

//            /////connecting to  the ussd database
//            $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
            $query = $this->db->query("select * from comprehensive_setting");
        
            $records['premiumsetting']=$query->result();
            $records['numreq']=$this->Numreq();
            $this->load->view('comprehensivesetting_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }


    public function updatereward(){
        /////connecting to  the ussd database

        $id=$this->input->post("id");
        $basic_premium=$this->input->post("basic_premium");
        $rate=$this->input->post("rate");

        $query = $this->db->query("UPDATE comprehensive_setting SET basic_premium = '$basic_premium', rate= '$rate' WHERE id = '$id' ");
        $_SESSION['success'] = 'success';
        redirect('comprehensivesetting');
    }
    
    
    ////mortor third party
    public function indexmotorthirdparty(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select * from motor,payments where motor.application_id  IN(select payments.application_id from payments)");
            $records['records'] = $query->result();
            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['numreq']=$this->Numreq();
//            /////connecting to  the ussd database
//            $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
            $query = $this->db->query("select * from motorthird_setting");

            $records['premiumsetting']=$query->result();
            $this->load->view('motorthirdpartyview',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
    public function updatemotorthirdparty(){
        /////connecting to  the ussd database

        $id=$this->input->post("id");
        $basic_premium=$this->input->post("basic_premium");
        $rate=$this->input->post("rate");

        $query = $this->db->query("UPDATE motorthird_setting SET basic_premium = '$basic_premium', rate= '$rate' WHERE id = '$id' ");
        $_SESSION['success'] = 'success';
        redirect('indexmotorthirdparty');
    }
    
    ///third party fire
    public function indexmotorthirdfireparty(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select * from motor,payments where motor.application_id  IN(select payments.application_id from payments)");
            $records['records'] = $query->result();
            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['numreq']=$this->Numreq();
//            /////connecting to  the ussd database
//            $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
            $query = $this->db->query("select * from thirdpartyfire_setting");

            $records['premiumsetting']=$query->result();
            $this->load->view('thirdfireview',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
    public function updatemotorthirdfireparty(){
        /////connecting to  the ussd database

        $id=$this->input->post("id");
        $basic_premium=$this->input->post("basic_premium");
        $rate=$this->input->post("rate");

        $query = $this->db->query("UPDATE thirdpartyfire_setting SET basic_premium = '$basic_premium', rate= '$rate' WHERE id = '$id' ");
        $_SESSION['success'] = 'success';
        redirect('indexmotorthirdfireparty');
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
