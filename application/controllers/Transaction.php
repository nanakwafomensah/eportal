<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');


        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    /*fire transaction*/
    public function indexfire()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select fire.application_id,fire.application_type,fire.* from fire,payments where 
                                        (fire.application_id    
                                        IN(select payments.application_id from payments) AND payments.status='A')");
            $records['records'] = $query->result();

            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['total_money']=$this->get_total_amount();
            $records['pending']=$this->get_total_amount_pending();
            $records['acted_on']=$this->get_total_amount_acted_on();
            $records['numreq']=$this->Numreq();
                $this->load->view('transactionViews/transactionFire_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
    public function indexuncompletedfire(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select fire.application_id,fire.application_type,fire.* from fire,payments where (fire.application_id IN(select payments.application_id from payments) AND payments.status='D')");
            $records['records'] = $query->result();

            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['numreq']=$this->Numreq();
             $this->load->view('transactionViews/uncomtransactionFire_view',$records);
          }else{
    //If no session, redirect to login page
        redirect('login', 'refresh');
        }
    }
    /*motor transaction*/
    public function indexmotor()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select motor.application_id,motor.application_type,payments.* from motor,payments where 
                                        (motor.application_id    
                                        IN(select payments.application_id from payments) AND payments.status='A')");
            $records['records'] = $query->result();

            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['total_money']=$this->get_total_amount();
            $records['pending']=$this->get_total_amount_pending();
            $records['acted_on']=$this->get_total_amount_acted_on();
            $records['numreq']=$this->Numreq();
            $this->load->view('transactionViews/transactionMotor_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
    public function indexuncompletedmotor(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $query = $this->db->query("select motor.application_id,motor.application_type,payments.* from motor,payments where (motor.application_id IN(select payments.application_id from payments) AND payments.status='D')");
            $records['records'] = $query->result();

            //$this->load->view('pay_view', $records);
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $records['numreq']=$this->Numreq();
            $this->load->view('/transactionViews/uncomtransactionMotor_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
















    public function querydatabase(){
        $this->load->database();
        $to_date=$this->input->post("to_date");
        $from_date=$this->input->post("from_date");
        $query = $this->db->query("select * from payments where date >= '$from_date' and date <= '$to_date'");

        $records['records'] = $query->result();
        echo json_encode($records);
    }

    public function get_basic_data(){
        $this->load->database();
        $to_date=$this->input->post("to_date");
        $from_date=$this->input->post("from_date");
        $s=$this->db->query("select sum(premium_amount) as c from payments where date >= '$from_date' and date <= '$to_date'");
        $result = $s->row();
//////////////////////////get_total_amount_pending///////////////////////
        $s=$this->db->query("select sum(premium_amount) as c from payments inner join motor  on payments.application_id=motor.application_id where motor.application_status='pending' and payments.date >= '$from_date' and payments.date <= '$to_date'");
        $t=$this->db->query("select sum(premium_amount) as c from payments inner join fire on payments.application_id=fire.application_id where fire.application_status='pending'  and payments.date >= '$from_date' and payments.date <= '$to_date'");

        $result1 = $s->row() ;
        $result2= $t->row() ;
        ////////////////////get_total_amount_acted_on////////////////////
        $s=$this->db->query("select sum(premium_amount) as c from payments inner join motor on payments.application_id=motor.application_id where motor.application_status='actedon' and payments.date >= '$from_date' and payments.date <= '$to_date'");
        $t=$this->db->query("select sum(premium_amount) as c from payments inner join fire on payments.application_id=fire.application_id where fire.application_status='actedon' and payments.date >= '$from_date' and payments.date <= '$to_date'");

        $result3 = $s->row() ;
        $result4= $t->row() ;
        echo $result->c."|".$result1->c ."|". $result2->c."|". $result3->c."|".$result4->c ;
    }

    public function get_total_amount(){
        $this->load->database();

        $s=$this->db->query("select sum(premium_amount) as c from payments ");
        $result = $s->row();
        return $result->c;
    }
    public function get_total_amount_pending(){
        $this->load->database();
        $s=$this->db->query("select sum(premium_amount) as c from payments inner join motor  on payments.application_id=motor.application_id where motor.application_status='pending'");
        $t=$this->db->query("select sum(premium_amount) as c from payments inner join fire on payments.application_id=fire.application_id where fire.application_status='pending'");

        $result1 = $s->row() ;
        $result2= $t->row() ;
        return $result1->c + $result2->c;
    }
    public function get_total_amount_acted_on(){
        $this->load->database();
        $s=$this->db->query("select sum(premium_amount) as c from payments inner join motor on payments.application_id=motor.application_id where motor.application_status='actedon'");
        $t=$this->db->query("select sum(premium_amount) as c from payments inner join fire on payments.application_id=fire.application_id where fire.application_status='actedon'");

        $result = $s->row() ;
        $result2= $t->row() ;
        return $result->c + $result2->c;


    }



    public  function get_network_stats_data(){
        $this->load->database();
        $to_date=$this->input->post("to_date");
        $from_date=$this->input->post("from_date");
        /////////////////////MTN/////////////////////////////////////

        //totalpayment
        $s=$this->db->query("select sum(premium_amount) as c from payments where mobile_network='MTN' and date >= '$from_date' and date <= '$to_date'");
        $result1mtn = $s->row();
       // $result1mtn = $result1mtn->c;
        //pending
        $s=$this->db->query("select sum(premium_amount) as c from payments inner join motor  on payments.application_id=motor.application_id where motor.application_status='pending' and mobile_network='MTN'  ");
        $t=$this->db->query("select sum(premium_amount) as c from payments inner join fire on payments.application_id=fire.application_id where fire.application_status='pending' and mobile_network='MTN' ");

        $result2mtn = $s->row() ;
        $result3mtn= $t->row();

       // $result2mtn = $result1->c + $result2->c;

        //actedon
        $s=$this->db->query("select sum(premium_amount) as c from payments inner join motor on payments.application_id=motor.application_id where motor.application_status='actedon' and mobile_network='MTN' ");
        $t=$this->db->query("select sum(premium_amount) as c from payments inner join fire on payments.application_id=fire.application_id where fire.application_status='actedon' and mobile_network='MTN' ");

        $result4mtn = $s->row() ;
        $result5mtn= $t->row() ;

       // $result3mtn = $result3+ $result4;

        echo $result1mtn->c."|".$result2mtn->c."|".$result3mtn->c."|".$result4mtn->c."|".$result5mtn->c;


    }

    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
