<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RequestedCall extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $this->db->select("*");
            $this->db->from("numbers");
    
            $query = $this->db->get();
    
            $records['records'] = $query->result();


            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            //$this->load->view('pay_view', $records);
            $records['numreq']=$this->Numreq();
            $this->load->view('requestedcall_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    public function switchcalls_made($number,$status){

          if($status=='A'){
              $data = array(
                  'status' => 'D'
        
              );
          }elseif ($status=='D'){
              $data = array(
                  'status' => 'A'

              );
          }
            
            //print_r($data) ;
            $this->db->where('number', $number);
            $this->db->update('numbers', $data);
            redirect('requestedcalls');

    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
