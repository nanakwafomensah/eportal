<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        
       
    }
    public function userutilindex(){
        if($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');

      
            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $query = $this->db->query("select users.*  from users  where users.status <> 'D'");
            $records['records'] = $query->result();
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
            $this->load->view('userutil_view', $records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
       
    }
    public function store(){
        //$this->session->userdata('logged_in');
        $session_data = $this->session->userdata('logged_in');
        $this->load->database();
        $lastupdateddate=date("Y-m-d");
        $lastupdatedtime=date("H:i:s");
        $lastupdatedip=$_SERVER['REMOTE_ADDR'];//get ip
        $password=12345;
        $_token=hash('sha512',$password);

        $data = array(
//            'protal'=>'EPORTAL',
            'usersfullname' => $this->input->post("fullname"),
            'sex' =>  $this->input->post("sex"),
            'useremail'=> $this->input->post("email"),

            'phonenumber'=>$this->input->post("phonenumber"),
           'role' =>  $this->input->post("userrole"),
//            'companyname'=>'loyalstar',
            'status' =>  'A',
            'lastupdateddate'=>$lastupdateddate,
            'lastupdatedtime'=>$lastupdatedtime,
            'lastupdatedip'=>$lastupdatedip,
            'datecreated'=>$lastupdateddate,
            'timecreated'=>$lastupdatedtime,
            'ipcreated'=>$lastupdatedip,
            'logintoken'=>$_token,
            'lastupdatedby'=>$session_data['username'],
            'lastseendate'=>$lastupdateddate,
            'lastseentime'=>$lastupdatedtime,
            'lastseenip'=>$lastupdatedip




        );

        $this->db->insert('users', $data);
        $name= $this->input->post("fullname");
        $auditlog= auditlog($session_data['username'],"USER CREATION","Created user $name");
        redirect('userutil', 'refresh');
    }

    public function updateuser(){
        $session_data = $this->session->userdata('logged_in');
        $this->load->database();
        $id=$this->input->post("userid_e");


        $data = array(
            'usersfullname' => $this->input->post("usersfullname_e"),
            'phonenumber' => $this->input->post("phonenumber_e"),
            'sex' => $this->input->post("sex_e"),
            'role' => $this->input->post("rolename_e")


        );
        $this->db->where('id', $id);
        $this->db->update('users',$data);

        $name= $this->input->post("usersfullname_e");
        $phonenumber=$this->input->post("phonenumber_e");
        $sex=$this->input->post("sex_e");
        $role=$this->input->post("rolename_e");

        $auditlog= auditlog($session_data['username'],"USER UPDATE","updated  $name ,$phonenumber,$sex,$role");
        $notification=array(
            'message'=>"User details Updated",
            'alert-type'=>'success'
        );

        $this->session->set_flashdata($notification);
        redirect('userutil');
    }

    public function deleteuser(){
        $this->load->database();
        $id=$this->input->post("user_de");


        $data = array(
            'status' => 'D'
        );
        $this->db->where('id', $id);
        $this->db->update('users',$data);
        $notification=array(
            'message'=>"User Deleted",
            'alert-type'=>'success'
        );

        $this->session->set_flashdata($notification);
        redirect('userutil');
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
       redirect('login', 'refresh');
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
