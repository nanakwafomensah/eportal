<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pay extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function indexmotor()
    {
        
            $this->load->view('paymentView/paymotor_view');

    }
    public function indexfire()
    {

        $this->load->view('paymentView/payfire_view');

    }
    public function manualpayment($token)
    {


        $data = array(
            'status' => 'A'

        );
        //print_r($data) ;
        $this->db->where('token', $token);
        $this->db->update('payments', $data);
        redirect('transaction');
    }
    public function savepaymentdata(){
        /////////////////////////////////////////////////////////////////
        //if api call succcessfull////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        $withdrawal_number=$this->input->post("withdrawal_number");
        $reward_number=$this->input->post("reward_number");
        $mobile_network=$this->input->post("mobile_network");
        $reward_type=$this->input->post("reward_type");
        $insurance_type=$this->input->post("insurance_type");
        $premium_amount=$this->input->post("premium_amount");
        $application_id=$this->input->post("application_number");
        $paymentstatus=$this->input->post("paymentstatus");
         //CALCULATE PERCENTAGE REWARD

        //get cashbackvalue
        $query = $this->db->query("select percentage as c from cashback ");
        $row = $query->row();
        $value=$row->c;
       // echo $row->percentage
        $percentage_reward= ( $value / 100) * $premium_amount;


        $data = array(
            'withdrawal_number'=>$withdrawal_number,
            'reward_number' => $reward_number,
            'mobile_network' => $mobile_network,
            'reward_type' => $reward_type,
            'insurance_type' => $insurance_type,
            'premium_amount'=> $premium_amount,
            'status' => 'D',
            'date' => date('Y-m-d'),
            'time' => date('h:i:s'),
            'token'=>mt_rand(100000, 999999),
            'percentage_reward'=>$percentage_reward,
            'application_id'=>$application_id,
            'paymentstatus'=>$paymentstatus

        );
        $this->db->insert('payments', $data);

        echo "saved";

    }
    public function clientpay(){
        $withdrawal_number=$this->input->post("withdrawal_number");
        $reward_number=$this->input->post("reward_number");
        $mobile_network=$this->input->post("mobile_network");
        $reward_type=$this->input->post("reward_type");
        $insurance_type=$this->input->post("insurance_type");
        $premium_amount=$this->input->post("premium_amount");
        $application_id=$this->input->post("application_number");
//hit payment debit api and send details

        $dateToPass = urlencode($withdrawal_number."|".$reward_number."|".$mobile_network."|".$reward_type."|".$insurance_type."|".$premium_amount."|".$application_id);

        $url="http://184.168.147.165:81/api/eportalApi/mobilemoney/debit.php?data=$dateToPass";

         file_get_contents($url);

        $home=base_url();
        $link='<strong><a href="'.$home.'">Go to Home</a></strong>';
        $this->session->set_flashdata('paymentMessage', "You would receive a prompt on your phone for confirmation Shortly \n Feel free to go back to the home page .$link");
        redirect('payprice');

    }
    public function storemotorinsurance(){
        $aplication_number=mt_rand(100000, 999999);
        $data = array(
            'application_id'=>$aplication_number,
            'name' => $this->input->post("name"),
            'dob' => $this->input->post("dob"),
            'mobilenumber' => $this->input->post("mobilenumber"),
            'occupation' => $this->input->post("occupation"),
            'posaddress' => $this->input->post("posaddress"),
            'phyaddress' => $this->input->post("phyaddress"),
            'email' => $this->input->post("email"),
            'tel' => $this->input->post("tel"),
            'from_' => $this->input->post("from_"),
            'at' => $this->input->post("at"),
            'to_' => $this->input->post("to_"),
            'type_of_cover' => $this->input->post("type_of_cover"),
            'tppd' => $this->input->post("tppd"),


            'make_model' => $this->input->post("make_model"),
            'type_of_body' => $this->input->post("type_of_body"),
            'cubic_capacity' => $this->input->post("cubic_capacity"),
            'no_of_seats' => $this->input->post("no_of_seats"),
            'year_of_manufacture' => $this->input->post("year_of_manufacture"),
            'value_of_vehicle' => $this->input->post("value_of_vehicle"),
            'value_of_accessories' => $this->input->post("value_of_accessories"),
            'reg_no' => $this->input->post("reg_no"),
            'chassis_no' => $this->input->post("chassis_no"),
            'engine_no' => $this->input->post("engine_no"),
            
            
            'about1' => $this->input->post("about1"),
            'about2' => $this->input->post("about2"),
            'about3' => $this->input->post("about3"),
            'about4' => $this->input->post("about4"),
            'about5' => $this->input->post("about5"),
            'about6' => $this->input->post("about6"),
            'about7' => $this->input->post("about7"),
            'about8' => $this->input->post("about8"),
            'about9' => $this->input->post("about9"),
            'about10' => $this->input->post("about10"),
            'about11' => $this->input->post("about11"),
            'about12' => $this->input->post("about12"),
            'about13' => $this->input->post("about13"),
            'about1text' => $this->input->post("about1text"),
            'about2text' => $this->input->post("about2text"),
            'about3text' => $this->input->post("about3text"),
            'about4text' => $this->input->post("about4text"),
            'about5text' => $this->input->post("about5text"),
            'about6text' => $this->input->post("about6text"),
            'about7text' => $this->input->post("about7text"),
            'about8text' => $this->input->post("about8text"),
            'about9text' => $this->input->post("about9text"),
            'about10text' => $this->input->post("about10text"),
            'about11text' => $this->input->post("about11text"),
            'about12text' => $this->input->post("about12text"),
            'about13text' => $this->input->post("about13text"),
            'application_date' => date('Y-m-d'),
            'application_time' => date('h:i:s'),

            
            'application_status'=>'pending'

        );
        $this->db->insert('motor', $data);
        $this->send_mail($this->input->post("email"),$this->input->post("name"),$aplication_number);
//      // trigger the calculation of the premium after the insert triger

        $type_of_cover=$this->input->post("type_of_cover");
        $no_of_seats=$this->input->post("no_of_seats");
        $cc_loading=$this->input->post("cubic_capacity");
        $tppdl=$this->input->post("tppd");
        $value_of_vehicle=$this->input->post("value_of_vehicle");
        $rate=$this->input->post("rate_1");
        $basic_premium=$this->input->post("basic_premium_1");
//
        $query = $this->db->query("sp_comprehensivePremium $type_of_cover,$aplication_number,$no_of_seats,$tppdl,$value_of_vehicle,$rate,$cc_loading,$basic_premium");


//
        //Send  the data
        $this->session->set_flashdata('insurance_type',$this->input->post("type_of_cover"));
        $this->session->set_flashdata('aplication_number',$aplication_number);


        redirect('payprice');

    }

    //query for details if application number do not exist
    public function querymotordetails(){

        $application_number = trim($this->input->post("application_number"));
        $s=$this->db->query("select type_of_cover from motor  where application_id = '$application_number'")->row();
        $d=$this->db->query("select premium from applications_premium  where application_id = '$application_number'")->row();

 if(!isset($d->premium)){
     $premium_amount='';
 }else{
     $premium_amount=$d->premium;
 }


        if(!isset($s->type_of_cover)){
            $type_of_cover='';
        }else{
            $type_of_cover=$s->type_of_cover;
        }



             echo $type_of_cover."|".$premium_amount;


    }

    public function storefireinsurance(){

        $aplication_number=mt_rand(100000, 999999);
        $data = array(
            'name' => $this->input->post("name"),
            'address' => $this->input->post("address"),
            'tel' => $this->input->post("tel"),
            'email' => $this->input->post("email"),
            'fax' => $this->input->post("fax"),

            'www' => $this->input->post("www"),
            'occupation' => $this->input->post("occupation"),

            'contact_person' => $this->input->post("contact_person"),
            'mobile' => $this->input->post("mobile"),
            'property_situated' => $this->input->post("property_situated"),
            'nature_of_contstruction' => $this->input->post("nature_of_contstruction"),

            'activity_premises' => $this->input->post("activity_premises"),
            'sufferred_loss_by_fire' => $this->input->post("sufferred_loss_by_fire"),
            'insurance_acceptance' => $this->input->post("insurance_acceptance"),
            'insured_by_another_company' => $this->input->post("insured_by_another_company"),
            'mortgage' => $this->input->post("mortgage"),
             'application_date' => date('Y-m-d'),
            'application_time' => date('h:i:s'),
             'application_status'=>'pending'



        );
        $this->db->insert('fire', $data);
        $this->send_mail($this->input->post("email"),$this->input->post("name"),$aplication_number);
//      // trigger the calculation of the premium after the insert triger
        //run stored procedure to calculate premium
        $query = $this->db->query("sp_firePremium $aplication_number");

        //Send  the data
        $this->session->set_flashdata('insurance_type','fire');
        $this->session->set_flashdata('aplication_number',$aplication_number);

        redirect('payprice');
    }
    public function callvisa_payment(){
        $amount=$this->input->post("amount"); 
        echo "http://184.168.147.165:81/eportalApi/visapayment/?amount=$amount" ;
        
    }
    public function send_mail($email_to,$applicantname ,$applicationnumber) {

        require_once(APPPATH.'third_party/PHPMailer/PHPMailerAutoload.php');
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "ssl";  // prefix for secure protocol to connect to the server
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 465;                   // SMTP port to connect to GMail
        $mail->Username   = "nanamensah1140@gmail.com";  // user email address
        $mail->Password   = "christembassey123";            // password in GMail
        $mail->SetFrom('nanamensah1140@gmail.com', 'Loyalty');  //Who is sending
        $mail->isHTML(true);
        $mail->Subject    = "Motor Insurance Application";
        $mail->Body      = "
            <html>
             <head>
                
            </head>
            <body>
            <h3>Hello ,$applicantname  </h3>
            <p>Your Application id is $applicationnumber</p><br>
           
            </body>
            </html>
        ";
        $destino = $email_to; // Who is addressed the email to
        $mail->AddAddress($destino, "Receiver");
        if(!$mail->Send()) {
            return false;
        } else {
            return true;
        }

    }


    public function calculate_premium(){


        $type_of_cover=$this->input->post("type_of_cover");
        $no_of_seats=$this->input->post("no_of_seats");
        $tppdl=$this->input->post("limit");
        $value_of_vehicle=$this->input->post("value_of_vehicle");
        $rate=$this->input->post("rate");
        $cc_loading=$this->input->post("cc_loading");
        $basic_premium=$this->input->post("basic_premium");

        $premium=$this->get_premium($type_of_cover,$no_of_seats,$tppdl,$value_of_vehicle,$rate,$cc_loading,$basic_premium);

    }
    public function Get_rate_basic_car_type(){
        $type_of_motor=trim($this->input->post("type_of_motor"));
        $type_of_cover=trim($this->input->post("type_of_cover"));
         if($type_of_cover=='comprehensive'){
             $rate=null;
             $basic_premium=null;

             $s=$this->db->query("select rate,basic_premium from comprehensive_setting where name='$type_of_motor'")->row();
             $rate=trim($s->rate);
             $basic_premium=trim($s->basic_premium);

             echo $rate."|".$basic_premium;
         }elseif( $type_of_cover=='motor_third_party'){
             $rate=null;
             $basic_premium=null;

             $s=$this->db->query("select rate,basic_premium from motorthird_setting where name='$type_of_motor'")->row();
             $rate=trim($s->rate);
             $basic_premium=trim($s->basic_premium);

             echo $rate."|".$basic_premium;
         }elseif ($type_of_cover=='third_party_fire_theft'){
             $rate=null;
             $basic_premium=null;

             $s=$this->db->query("select rate,basic_premium from thirdpartyfire_setting where name='$type_of_motor'")->row();
             $rate=trim($s->rate);
             $basic_premium=trim($s->basic_premium);

             echo $rate."|".$basic_premium;
         }



    }
    public function get_premium($type_of_cover,$no_of_seats,$tppdl,$value_of_vehicle,$rate,$cc_loading,$basic_premium){


        if($type_of_cover=='comprehensive'){
            //        CALCULATE SEATS VALUE
            $no_of_seats_base=5;
            $seats_value=null;
            if($no_of_seats > $no_of_seats_base){
                $seats_value=($no_of_seats - $no_of_seats_base)*5.00;

            }else{
                $seats_value=(0)* 5.00;

            }
            //        CALCULATE DAMAGE VALUE
            $damage=null;
            //$damage=$value_of_vehicle * 0.05;
            $damage=$value_of_vehicle * ($rate / 100);
           //$comprehensive_basic=$damage + $seats_value + 320.00;
            $comprehensive_basic=$damage + $seats_value + $basic_premium;

            //         CALCULATE  NCD VALUE
            $ncd=null;
            $ncd = $comprehensive_basic * 0.50; //50%

            ///        CALCULATE  EXTRA VALUE
            $extra = ($tppdl - 2000) * 0.015; //1.5%

            ///       CALCULATE  PREMIUM VALUE
            $total_premium = $ncd + 50.00 + $extra;

//          echo 'GHS '.number_format($total_premium);
        }
        elseif( $type_of_cover=='motor_third_party'){

            $tp_basic_premium=$basic_premium;
            $cc_loading_val = $basic_premium * ($cc_loading/100);
            $old_age_loading =$basic_premium * ($cc_loading/100);
            $Total_basic_premium = $tp_basic_premium + $cc_loading_val + $old_age_loading;

            $rate=($rate / 100);
            $total_premium= ($rate * $value_of_vehicle) + $Total_basic_premium;


        }
        elseif ($type_of_cover=='third_party_fire_theft'){
            $total_premium=0.00;
        }

        echo 'GHS '.$total_premium;
    }

    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
