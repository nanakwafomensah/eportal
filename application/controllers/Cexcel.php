<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cexcel extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('excel');
        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }
    }


    public function excel($from_date,$to_date)
    {
        $from_date= preg_replace('/^%22/','',$from_date);
        $from_date= substr($from_date, 0, 10);

        $to_date= preg_replace('/^%22/','',$to_date);
        $to_date= substr($to_date, 0, 10);

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle($from_date .'-'. $to_date);
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'Transaction between '.$from_date .'-'. $to_date );
        $this->excel->getActiveSheet()->setCellValue('A3', 'S.No.');
        $this->excel->getActiveSheet()->setCellValue('B3', 'Status');
        $this->excel->getActiveSheet()->setCellValue('C3', 'Date');
        $this->excel->getActiveSheet()->setCellValue('D3', 'Time');
        $this->excel->getActiveSheet()->setCellValue('E3', 'Token');
        $this->excel->getActiveSheet()->setCellValue('F3', 'Premium Amount');
        $this->excel->getActiveSheet()->setCellValue('G3', 'Reward Type');
        $this->excel->getActiveSheet()->setCellValue('H3', 'Insurance Type');
        $this->excel->getActiveSheet()->setCellValue('I3', 'Mobile Network');
        $this->excel->getActiveSheet()->setCellValue('J3', 'Reward Number');
        $this->excel->getActiveSheet()->setCellValue('K3', 'Withdrawal Number');
        $this->excel->getActiveSheet()->setCellValue('L3', 'Percentage Reward');
        $this->excel->getActiveSheet()->setCellValue('M3', 'Application ID');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:C1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
        for($col = ord('A'); $col <= ord('C'); $col++){ //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
            //change the font size
            $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);

            $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        //retrive  table data
        $this->db->select("*");
        $this->db->from("payments");
        $this->db->where('date >=',$from_date);
        $this->db->where('date >=',$to_date);
        $rs = $this->db->get();
        $exceldata="";
        foreach ($rs->result_array() as $row){
            $exceldata[] = $row;
        }
        
        //Fill data
        $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');

        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename='Loyal_trans.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

    }

    public function  couponreward($from_date,$to_date){
        $from_date= preg_replace('/^%22/','',$from_date);
        $from_date= substr($from_date, 0, 10);

        $to_date= preg_replace('/^%22/','',$to_date);
        $to_date= substr($to_date, 0, 10);

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle($from_date .'-'. $to_date);
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'REWARDS BETWEEN '.$from_date .'-'. $to_date );
        $this->excel->getActiveSheet()->setCellValue('A3', 'PIN');
        $this->excel->getActiveSheet()->setCellValue('B3', 'COUPON');
        $this->excel->getActiveSheet()->setCellValue('C3', 'REWARD');
        $this->excel->getActiveSheet()->setCellValue('D3', 'PHONE NUMBER');
        $this->excel->getActiveSheet()->setCellValue('E3', 'DONATION');
        $this->excel->getActiveSheet()->setCellValue('F3', 'INSURANCE TYPE');
        $this->excel->getActiveSheet()->setCellValue('G3', 'DATE REDEEMED');
        $this->excel->getActiveSheet()->setCellValue('H3', 'RECEIPT/VEHICLE NUMBER');
        $this->excel->getActiveSheet()->setCellValue('I3', 'FUEL STATION');
//        $this->excel->getActiveSheet()->setCellValue('J3', 'FUEL STATION');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:C1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
        for($col = ord('A'); $col <= ord('C'); $col++){ //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
            //change the font size
            $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);

            $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        //retrive  table data
        $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
        $query = $otherdb->query("select agent_pin,coupon,reward,agent_phonenumber,donation_value,insurance_type,dateredeemed,agent_receiptnumber,insuranceagentpins.fuelstation from coupon_reward_reg  inner join insuranceagentpins on coupon_reward_reg.agent_pin =insuranceagentpins.agentpin WHERE coupon_reward_reg.dateredeemed >='$from_date' and coupon_reward_reg.dateredeemed <='$to_date' ");
        $rs = $query;
        $exceldata="";
        foreach ($rs->result_array() as $row){
            $exceldata[] = $row;
        }

        //Fill data
        $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');

        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename='couponreward.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function  mm_cashback($from_date,$to_date){
        $from_date= preg_replace('/^%22/','',$from_date);
        $from_date= substr($from_date, 0, 10);

        $to_date= preg_replace('/^%22/','',$to_date);
        $to_date= substr($to_date, 0, 10);

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle($from_date .'-'. $to_date);
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'CASHBACK -REWARDS BETWEEN '.$from_date .'-'. $to_date );
        $this->excel->getActiveSheet()->setCellValue('A3', 'PIN');
        $this->excel->getActiveSheet()->setCellValue('B3', 'MM NETWORK');
        $this->excel->getActiveSheet()->setCellValue('C3', 'MM NUMBER');
        $this->excel->getActiveSheet()->setCellValue('D3', 'RECEIPT / CAR NUMBER');
        $this->excel->getActiveSheet()->setCellValue('E3', 'DONATION');
        $this->excel->getActiveSheet()->setCellValue('F3', 'REWARD');
        $this->excel->getActiveSheet()->setCellValue('G3', 'INSURANCE TYPE');
        $this->excel->getActiveSheet()->setCellValue('H3', 'AGENT PHONENUMBER');
        $this->excel->getActiveSheet()->setCellValue('I3', 'FUEL STATION');
//        $this->excel->getActiveSheet()->setCellValue('J3', 'FUEL STATION');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:C1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
        for($col = ord('A'); $col <= ord('C'); $col++){ //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
            //change the font size
            $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);

            $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        //retrive  table data
        $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
        $query = $otherdb->query("select agent_pin,mm_network,mm_number,agent_receiptnumber,donation_value,reward,insurance_type,agent_phonenumber,insuranceagentpins.fuelstation from money_reward_reg
  inner join insuranceagentpins on money_reward_reg.agent_pin =insuranceagentpins.agentpin WHERE money_reward_reg.dategen >='$from_date' and money_reward_reg.dategen <='$to_date'");
        $rs = $query;
        $exceldata="";
        foreach ($rs->result_array() as $row){
            $exceldata[] = $row;
        }

        //Fill data
        $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');

        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename='cashback.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    public function airtime($from_date,$to_date){
        $from_date= preg_replace('/^%22/','',$from_date);
        $from_date= substr($from_date, 0, 10);

        $to_date= preg_replace('/^%22/','',$to_date);
        $to_date= substr($to_date, 0, 10);

        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle($from_date .'-'. $to_date);
        //set cell A1 content with some text
        $this->excel->getActiveSheet()->setCellValue('A1', 'AIRTIME REWARDS BETWEEN '.$from_date .'-'. $to_date );
        $this->excel->getActiveSheet()->setCellValue('A3', 'PIN');
        $this->excel->getActiveSheet()->setCellValue('B3', 'MM NETWORK');
        $this->excel->getActiveSheet()->setCellValue('C3', 'MM NUMBER');
        $this->excel->getActiveSheet()->setCellValue('D3', 'RECEIPT / CAR NUMBER');
        $this->excel->getActiveSheet()->setCellValue('E3', 'DONATION');
        $this->excel->getActiveSheet()->setCellValue('F3', 'REWARD');
        $this->excel->getActiveSheet()->setCellValue('G3', 'INSURANCE TYPE');
        $this->excel->getActiveSheet()->setCellValue('H3', 'AGENT PHONENUMBER');
        $this->excel->getActiveSheet()->setCellValue('I3', 'FUEL STATION');
//        $this->excel->getActiveSheet()->setCellValue('J3', 'FUEL STATION');
        //merge cell A1 until C1
        $this->excel->getActiveSheet()->mergeCells('A1:C1');
        //set aligment to center for that merged cell (A1 to C1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
        $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
        for($col = ord('A'); $col <= ord('C'); $col++){ //set column dimension $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
            //change the font size
            $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);

            $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        //retrive  table data
        $otherdb = $this->load->database('otherdb', TRUE); // the TRUE paramater tells CI that you'd like to return the database object.
        $query = $otherdb->query("select agent_pin,airtime_reward_network,airtime_reward_number,agent_receiptnumber,donation_value,reward,insurance_type,agent_phonenumber,insuranceagentpins.fuelstation from airtime_reward_reg
  inner join insuranceagentpins on airtime_reward_reg.agent_pin =insuranceagentpins.agentpin WHERE airtime_reward_reg.dategen >='$from_date' and airtime_reward_reg.dategen <='$to_date'");
        $rs = $query;
        $exceldata="";
        foreach ($rs->result_array() as $row){
            $exceldata[] = $row;
        }

        //Fill data
        $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');

        $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $filename='airtimerewards.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */