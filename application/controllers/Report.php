<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');


        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }
    }


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function transcompletedquery()
    {
        $fromdate=$this->input->post('dateFrom');
        $todate=$this->input->post('dateTo');

        $this->session->set_flashdata('fromdate', $fromdate);
        $this->session->set_flashdata('todate', $todate);

        //flash data
        redirect('transcompletedrpt');
        
        
      


    }
    
    public function untranscompletedquery(){

        $fromdate=$this->input->post('dateFrom');
        $todate=$this->input->post('dateTo');

        $this->session->set_flashdata('fromdate', $fromdate);
        $this->session->set_flashdata('todate', $todate);

        //flash data
        redirect('untranscompletedrpt');
    }
    
    
    public function transactionview(){
        if($this->session->userdata('logged_in')) {

            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;

            $fromdate=$this->session->flashdata('fromdate');
            $todate=$this->session->flashdata('todate');
            $records['fromdate'] = $fromdate;
            $records['todate'] = $todate;


//        ////////////////////TOTAL
//        $s=$this->db->query("select sum(amount) as total from trans  where datetrans >='$fromdate' and datetrans <='$todate'");
//        if($s->num_rows()){
//            $result = $s->row();
//            $amount=$result->total;
//            $records['total'] = $amount;
//        }else{
//            $amount= '0.00';
//            $records['total'] = $amount;
//        }
//        ////////////////PENDING
//        $s=$this->db->query("select sum(amount) as total from trans WHERE status='U' and datetrans >='$fromdate' and datetrans <='$todate' ");
//        if($s->num_rows()){
//            $result = $s->row();
//            $amount=$result->total;
//            $records['pending'] = $amount;
//        }else{
//            $amount= '0.00';
//            $records['pending'] = $amount;
//        }
//        ////////////////processed
//        $s=$this->db->query("select sum(amount) as total from trans WHERE status='P' and datetrans >='$fromdate' and datetrans <='$todate' ");
//        if($s->num_rows()){
//            $result = $s->row();
//            $amount=$result->total;
//            $records['processed'] = $amount;
//        }else{
//            $amount= '0.00';
//            $records['processed'] = $amount;
//        }
//
            $query = $this->db->query("select * from motor left join payments on motor.application_id=payments.application_id
                                        where motor.application_id NOT IN (select application_id  from payments)");
            $records['records'] = $query->result();
            $records['numreq']=$this->Numreq();

        $this->load->view('transaction_rpt/transactionrpt_view',$records);

        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }


    public function uncomtransactionview(){
        if($this->session->userdata('logged_in')) {

            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];

            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;

            $fromdate=$this->session->flashdata('fromdate');
            $todate=$this->session->flashdata('todate');
            $records['fromdate'] = $fromdate;
            $records['todate'] = $todate;


//        ////////////////////TOTAL
//        $s=$this->db->query("select sum(amount) as total from trans  where datetrans >='$fromdate' and datetrans <='$todate'");
//        if($s->num_rows()){
//            $result = $s->row();
//            $amount=$result->total;
//            $records['total'] = $amount;
//        }else{
//            $amount= '0.00';
//            $records['total'] = $amount;
//        }
//        ////////////////PENDING
//        $s=$this->db->query("select sum(amount) as total from trans WHERE status='U' and datetrans >='$fromdate' and datetrans <='$todate' ");
//        if($s->num_rows()){
//            $result = $s->row();
//            $amount=$result->total;
//            $records['pending'] = $amount;
//        }else{
//            $amount= '0.00';
//            $records['pending'] = $amount;
//        }
//        ////////////////processed
//        $s=$this->db->query("select sum(amount) as total from trans WHERE status='P' and datetrans >='$fromdate' and datetrans <='$todate' ");
//        if($s->num_rows()){
//            $result = $s->row();
//            $amount=$result->total;
//            $records['processed'] = $amount;
//        }else{
//            $amount= '0.00';
//            $records['processed'] = $amount;
//        }
//
            $query = $this->db->query("select * from motor left join payments on motor.application_id=payments.application_id
                                        where motor.application_id NOT IN (select application_id  from payments)");
            $records['records'] = $query->result();
            $records['numreq']=$this->Numreq();
            $this->load->view('transaction_rpt/uncomtransactionrpt_view',$records);

        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
}
