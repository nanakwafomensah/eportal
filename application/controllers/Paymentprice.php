<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentprice extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $insurance_type = $this->session->flashdata('insurance_type');
        $aplication_number= $this->session->flashdata('aplication_number');
        
        if(isset($insurance_type)&&isset($aplication_number))
        {
            ///select premium amount
            $s=$this->db->query("select premium from applications_premium where application_id='$aplication_number'")->row();
            
            $records['premium_amount']=$s->premium;
            $records['insurance_type']=$insurance_type;
            $records['aplication_number']=$aplication_number;
            //MOBILE PAYMENT MESSAGE
            $records['paymentMessage']=$this->session->flashdata('paymentMessage');
            $records['numreq']=$this->Numreq();
            $this->load->view('payment_price_view',$records);
        }else{
          
            $records['aplication_number']='';
            //MOBILE PAYMENT MESSAGE
            $records['paymentMessage']=$this->session->flashdata('paymentMessage');
            $this->load->view('payment_price_view',$records);
        }



    }
    public function Numreq(){
        $this->db->select("*");
        $this->db->from("numbers");
        $this->db->where('status','D');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }

}
