<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index_view.php/welcome
     *	- or -
     * 		http://example.com/index_view.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index_view.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $s=$this->db->query("select id,percentage from cashback ");
            $result = $s->row();
            $records['percentage']=$result->percentage;
            $records['identifervalue']=$result->id;
///////////////////////////////dashboard data/////////////////////////////////
            $records['no_of_backendusers']= $this->no_of_backenduser();
            $records['no_of_attempted_payment']= $this->no_of_attempted_payment();
            $records['revenue']= $this->revenue();
            $records['numreq']=$this->Numreq();
           
////////////////////////////////////////////////////////////////////////////
            $this->load->view('analytic_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
public function Numreq(){
    $this->db->select("*");
    $this->db->from("numbers");
    $this->db->where('status','D');
    $query = $this->db->get();

    $value = $query->num_rows();
    return $value;
}
    public function no_of_backenduser(){
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where('status','A');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }
    public function no_of_attempted_payment(){///Today
        $yeard=date('y');

        $this->db->select("*");
        $this->db->from("payments");
        $this->db->where('date like','%'.$yeard.'%');
        $query = $this->db->get();

        $value = $query->num_rows();
        return $value;
    }

    public function revenue(){//This year
        $yeard=date('y');

        $this->load->database();
        $s=$this->db->query("select sum(premium_amount) as c from payments where status='A' and paymentstatus='001' and date like '%$yeard%'");
        $result = $s->row();
        return $result->c ;

    }

    public function active_inactive(){
        ///////active
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where('status','A');
        $aquery = $this->db->get();
        $avalue = $aquery->num_rows();

        /////////inactiev
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where('status','D');
        $iquery = $this->db->get();
        $ivalue = $iquery->num_rows();

        echo $avalue.'|'.$ivalue;
    }


    public function transaction_graph_montly(){
        $this->load->database();
        $pyear=date('Y');
        $jandate=$pyear.'-'.'01';
        $febdate=$pyear.'-'.'02';
        $mardate=$pyear.'-'.'03';
        $aprdate=$pyear.'-'.'04';
        $maydate=$pyear.'-'.'05';
        $jundate=$pyear.'-'.'06';
        $juldate=$pyear.'-'.'07';
        $augdate=$pyear.'-'.'08';
        $sepdate=$pyear.'-'.'09';
        $octdate=$pyear.'-'.'10';
        $novdate=$pyear.'-'.'11';
        $decdate=$pyear.'-'.'12';

 //  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$jandate%'");
        $resulttotalpayment = $stotalpayment->row();
        $jan_payment=$resulttotalpayment->c ;

///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$jandate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $jan_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$jandate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $jan_pending=$pendingtotalpayment->c ;
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$febdate%'");
        $resulttotalpayment = $stotalpayment->row();
        $feb_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$febdate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $feb_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$febdate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $feb_pending=$pendingtotalpayment->c ;

        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$mardate%'");
        $resulttotalpayment = $stotalpayment->row();
        $mar_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$mardate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $mar_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$mardate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $mar_pending=$pendingtotalpayment->c ;


        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$aprdate%'");
        $resulttotalpayment = $stotalpayment->row();
        $apr_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$aprdate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $apr_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$aprdate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $apr_pending=$pendingtotalpayment->c ;

        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$maydate%'");
        $resulttotalpayment = $stotalpayment->row();
        $may_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$maydate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $may_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$maydate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $may_pending=$pendingtotalpayment->c ;
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$jundate%'");
        $resulttotalpayment = $stotalpayment->row();
        $jun_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$jundate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $jun_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$jundate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $jun_pending=$pendingtotalpayment->c ;

        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$juldate%'");
        $resulttotalpayment = $stotalpayment->row();
        $jul_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$juldate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $jul_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$juldate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $jul_pending=$pendingtotalpayment->c ;
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$augdate%'");
        $resulttotalpayment = $stotalpayment->row();
        $aug_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$augdate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $aug_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$augdate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $aug_pending=$pendingtotalpayment->c ;

        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$sepdate%'");
        $resulttotalpayment = $stotalpayment->row();
        $sep_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$sepdate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $sep_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$sepdate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $sep_pending=$pendingtotalpayment->c ;

        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$octdate%'");
        $resulttotalpayment = $stotalpayment->row();
        $oct_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$octdate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $oct_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$octdate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $oct_pending=$pendingtotalpayment->c ;

        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$novdate%'");
        $resulttotalpayment = $stotalpayment->row();
        $nov_payment=$resulttotalpayment->c ;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$novdate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $nov_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$novdate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $nov_pending=$pendingtotalpayment->c ;
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////
//  totalpayment
        $stotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and date like '%$decdate%'");
        $resulttotalpayment = $stotalpayment->row();
        $dec_payment=$resulttotalpayment->c;
///acted on

        $actedontotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status='A' and date like '%$decdate%'");
        $actedontotalpayment = $actedontotalpayment->row();
        $dec_actedon=$actedontotalpayment->c ;

///pending
        $pendingtotalpayment=$this->db->query("select count(*) as c from payments where paymentstatus='001' and status ='D' and date like '%$decdate%'");
        $pendingtotalpayment = $pendingtotalpayment->row();
        $dec_pending=$pendingtotalpayment->c ;

        echo $jan_payment."|".$jan_actedon."|".$jan_pending
        ."|".$feb_payment."|".$feb_actedon."|".$feb_pending
        ."|".$mar_payment."|".$mar_actedon."|".$mar_pending
        ."|".$apr_payment."|".$apr_actedon."|".$apr_pending
        ."|".$may_payment."|".$may_actedon."|".$may_pending
        ."|".$jun_payment."|".$jun_actedon."|".$jun_pending
        ."|".$jul_payment."|".$jul_actedon."|".$jul_pending
        ."|".$aug_payment."|".$aug_actedon."|".$aug_pending
        ."|".$sep_payment."|".$sep_actedon."|".$sep_pending
        ."|".$oct_payment."|".$oct_actedon."|".$oct_pending
        ."|".$nov_payment."|".$nov_actedon."|".$nov_pending
        ."|".$dec_payment."|".$dec_actedon."|".$dec_pending;
    }

   


}
